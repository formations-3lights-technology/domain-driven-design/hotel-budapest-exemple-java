package com.threelightstechnology.hotelbudapest.deroulement_sejour.unit;

import com.threelightstechnology.hotelbudapest.deroulement_sejour.domain.reservation.Reservation;
import com.threelightstechnology.hotelbudapest.deroulement_sejour.domain.reservation.ReservationLoader;
import com.threelightstechnology.hotelbudapest.deroulement_sejour.domain.sejour.events.EnvoiFactureDemandeEvent;
import com.threelightstechnology.hotelbudapest.deroulement_sejour.use_cases.finir_sejour.event_listeners.EnvoyerUneFactureEventListener;
import com.threelightstechnology.hotelbudapest.shared.domain.http_client.HttpClient;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Optional;

import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.verify;

@ExtendWith(MockitoExtension.class)
public class EnvoyerUneFactureEventListenerTest {
    @Mock
    private ReservationLoader reservationLoader;
    @Mock
    private HttpClient httpClient;

    private EnvoyerUneFactureEventListener underTest;

    @BeforeEach
    void setUp() {
        underTest = new EnvoyerUneFactureEventListener(reservationLoader, httpClient);
    }

    @Test
    void Mail_non_specifie() {
        // GIVEN
        given(reservationLoader.getParIdOuErreur("reservation-id")).willReturn(
                new Reservation(
                        "reservation-id",
                        "reservation@email.com",
                        new BigDecimal(50),
                        LocalDateTime.of(2023, 10, 16, 9, 18, 0),
                        LocalDateTime.of(2023, 10, 17, 9, 18, 0)
                )
        );
        EnvoiFactureDemandeEvent event = new EnvoiFactureDemandeEvent(
                "sejour-id",
                "reservation-id",
                Optional.empty(),
                new BigDecimal(200)
        );

        // WHEN
        underTest.listen(event);

        // THEN
        verify(httpClient).post(
                "/invoices/send",
                Optional.of("{invoiceEmail='reservation@email.com', amount=200}")
        );
    }

    @Nested
    public class ToutSePasseBien {
        @Test
        void Mail_specifie() {
            // GIVEN
            EnvoiFactureDemandeEvent event = new EnvoiFactureDemandeEvent(
                    "sejour-id",
                    "reservation-id",
                    Optional.of("facture@email.com"),
                    new BigDecimal(200)
            );

            // WHEN
            underTest.listen(event);

            // THEN
            verify(httpClient).post(
                    "/invoices/send",
                    Optional.of("{invoiceEmail='facture@email.com', amount=200}")
            );
        }
    }
}
