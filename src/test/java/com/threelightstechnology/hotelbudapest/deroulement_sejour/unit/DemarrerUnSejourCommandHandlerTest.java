package com.threelightstechnology.hotelbudapest.deroulement_sejour.unit;

import com.threelightstechnology.hotelbudapest.deroulement_sejour.domain.reservation.Reservation;
import com.threelightstechnology.hotelbudapest.deroulement_sejour.domain.reservation.ReservationLoader;
import com.threelightstechnology.hotelbudapest.deroulement_sejour.domain.reservation.ReservationNonTrouveeException;
import com.threelightstechnology.hotelbudapest.deroulement_sejour.domain.sejour.SejourRepository;
import com.threelightstechnology.hotelbudapest.deroulement_sejour.domain.sejour.SejourSnapshot;
import com.threelightstechnology.hotelbudapest.deroulement_sejour.domain.sejour.events.SejourEnregistreEvent;
import com.threelightstechnology.hotelbudapest.deroulement_sejour.domain.sejour.exceptions.SejourEnregistrementTropTardException;
import com.threelightstechnology.hotelbudapest.deroulement_sejour.use_cases.demarrer_sejour.DemarrerUnSejourCommand;
import com.threelightstechnology.hotelbudapest.deroulement_sejour.use_cases.demarrer_sejour.DemarrerUnSejourCommandHandler;
import com.threelightstechnology.hotelbudapest.shared.event_publisher.EventPublisher;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.verify;

@ExtendWith(MockitoExtension.class)
public class DemarrerUnSejourCommandHandlerTest {
    @Mock
    private SejourRepository sejourRepository;
    @Mock
    private ReservationLoader reservationLoader;
    @Mock
    private EventPublisher eventPublisher;

    private DemarrerUnSejourCommandHandler underTest;

    @BeforeEach
    void setUp() {
        underTest = new DemarrerUnSejourCommandHandler(sejourRepository, reservationLoader, eventPublisher);
    }

    @Test
    void Tout_se_passe_bien() {
        // GIVEN
        given(sejourRepository.nextId()).willReturn("next-id");
        given(reservationLoader.getParIdOuErreur("reservation-id")).willReturn(
                new Reservation(
                        "reservation-id",
                        "reservation@email.com",
                        new BigDecimal(50),
                        LocalDateTime.of(2023, 10, 16, 9, 18, 0),
                        LocalDateTime.of(2023, 10, 17, 9, 18, 0)
                )
        );
        DemarrerUnSejourCommand command = new DemarrerUnSejourCommand(
                "reservation-id",
                LocalDateTime.of(2023, 10, 16, 9, 18, 0)
        );

        // WHEN
        underTest.handle(command);

        // THEN
        verify(sejourRepository).save(
                new SejourSnapshot(
                        "next-id",
                        "reservation-id",
                        new BigDecimal(50),
                        LocalDateTime.of(2023, 10, 16, 9, 18, 0),
                        LocalDateTime.of(2023, 10, 17, 9, 18, 0),
                        Optional.empty(),
                        Optional.empty()
                )
        );
        verify(eventPublisher).publish(List.of(
                new SejourEnregistreEvent(
                        "next-id",
                        "reservation-id",
                        LocalDateTime.of(2023, 10, 16, 9, 18, 0)
                )
        ));
    }

    @Nested
    public class InformerQuandIlYAUneErreur {
        @Test
        void La_reservation_n_existe_pas() {
            // GIVEN
            doThrow(new ReservationNonTrouveeException()).when(reservationLoader).getParIdOuErreur("inconnue");
            DemarrerUnSejourCommand command = new DemarrerUnSejourCommand(
                    "inconnue",
                    LocalDateTime.of(2023, 10, 16, 9, 18, 0)
            );

            // WHEN
            assertThatThrownBy(() -> underTest.handle(command))
                    // THEN
                    .isInstanceOf(ReservationNonTrouveeException.class)
                    .hasMessage("Booking is not found");
        }

        @Test
        void Il_est_trop_tard() {
            // GIVEN
            given(reservationLoader.getParIdOuErreur("reservation-id")).willReturn(
                    new Reservation(
                            "reservation-id",
                            "reservation@email.com",
                            new BigDecimal(50),
                            LocalDateTime.of(2023, 10, 16, 9, 18, 0),
                            LocalDateTime.of(2023, 10, 30, 9, 18, 0)
                    )
            );
            DemarrerUnSejourCommand command = new DemarrerUnSejourCommand(
                    "reservation-id",
                    LocalDateTime.of(2023, 10, 18, 9, 18, 0)
            );

            // WHEN
            assertThatThrownBy(() -> underTest.handle(command))
                    // THEN
                    .isInstanceOf(SejourEnregistrementTropTardException.class)
                    .hasMessage("Check in is too late");
        }
    }
}
