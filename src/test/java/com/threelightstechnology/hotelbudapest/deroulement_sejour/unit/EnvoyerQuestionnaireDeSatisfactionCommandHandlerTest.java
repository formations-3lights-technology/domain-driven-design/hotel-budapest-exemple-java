package com.threelightstechnology.hotelbudapest.deroulement_sejour.unit;

import com.threelightstechnology.hotelbudapest.deroulement_sejour.domain.reservation.Reservation;
import com.threelightstechnology.hotelbudapest.deroulement_sejour.domain.reservation.ReservationLoader;
import com.threelightstechnology.hotelbudapest.deroulement_sejour.domain.sejour.SejourRepository;
import com.threelightstechnology.hotelbudapest.deroulement_sejour.domain.sejour.SejourSnapshot;
import com.threelightstechnology.hotelbudapest.deroulement_sejour.use_cases.envoyer_questionnaire_de_satisfaction.EnvoyerQuestionnaireDeSatisfactionCommand;
import com.threelightstechnology.hotelbudapest.deroulement_sejour.use_cases.envoyer_questionnaire_de_satisfaction.EnvoyerQuestionnaireDeSatisfactionCommandHandler;
import com.threelightstechnology.hotelbudapest.facturation.domain.Mail;
import com.threelightstechnology.hotelbudapest.facturation.domain.MailSender;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

@ExtendWith(MockitoExtension.class)
public class EnvoyerQuestionnaireDeSatisfactionCommandHandlerTest {
    @Mock
    private SejourRepository sejourRepository;
    @Mock
    private ReservationLoader reservationLoader;
    @Mock
    private MailSender mailSender;

    private EnvoyerQuestionnaireDeSatisfactionCommandHandler underTest;

    @BeforeEach
    void setUp() {
        underTest = new EnvoyerQuestionnaireDeSatisfactionCommandHandler(sejourRepository, reservationLoader, mailSender);
    }

    @Test
    void Tout_se_passe_bien() {
        // GIVEN
        given(sejourRepository.getParDateDeFin(LocalDate.of(2023, 10, 19))).willReturn(List.of(
                new SejourSnapshot(
                        "sejour-id",
                        "reservation-id",
                        new BigDecimal(50),
                        LocalDateTime.of(2023, 10, 16, 9, 18),
                        LocalDateTime.of(2023, 10, 20, 9, 18),
                        Optional.of(LocalDateTime.of(2023, 10, 19, 9, 18)),
                        Optional.of(new BigDecimal(100))
                ),
                new SejourSnapshot(
                        "sejour-id-2",
                        "reservation-id-2",
                        new BigDecimal(50),
                        LocalDateTime.of(2023, 10, 16, 9, 18),
                        LocalDateTime.of(2023, 10, 20, 9, 18),
                        Optional.of(LocalDateTime.of(2023, 10, 19, 9, 18)),
                        Optional.of(new BigDecimal(100))
                )
        ));
        given(reservationLoader.getParIdOuErreur("reservation-id")).willReturn(
                new Reservation(
                        "reservation-id",
                        "reservation@email.com",
                        new BigDecimal(50),
                        LocalDateTime.of(2023, 10, 16, 9, 18),
                        LocalDateTime.of(2023, 10, 17, 9, 18)
                )
        );
        given(reservationLoader.getParIdOuErreur("reservation-id-2")).willReturn(
                new Reservation(
                        "reservation-id-2",
                        "reservation-2@email.com",
                        new BigDecimal(50),
                        LocalDateTime.of(2023, 10, 16, 9, 18),
                        LocalDateTime.of(2023, 10, 17, 9, 18)
                )
        );

        EnvoyerQuestionnaireDeSatisfactionCommand command = new EnvoyerQuestionnaireDeSatisfactionCommand(LocalDateTime.of(2023, 10, 19, 9, 18));

        // WHEN
        underTest.handle(command);

        // THEN
        verify(mailSender, times(1)).send(
                new Mail(
                        "relation-client@email.com",
                        "reservation@email.com",
                        "Questionnaire de satisfaction"
                )
        );
        verify(mailSender, times(1)).send(
                new Mail(
                        "relation-client@email.com",
                        "reservation-2@email.com",
                        "Questionnaire de satisfaction"
                )
        );
    }
}
