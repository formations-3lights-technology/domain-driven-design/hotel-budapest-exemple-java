package com.threelightstechnology.hotelbudapest.deroulement_sejour.unit;

import com.threelightstechnology.hotelbudapest.deroulement_sejour.domain.sejour.SejourRepository;
import com.threelightstechnology.hotelbudapest.deroulement_sejour.domain.sejour.SejourSnapshot;
import com.threelightstechnology.hotelbudapest.deroulement_sejour.domain.sejour.exceptions.SejourNonTrouveException;
import com.threelightstechnology.hotelbudapest.deroulement_sejour.use_cases.GetSejourParId;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.doThrow;

@ExtendWith(MockitoExtension.class)
public class GetSejourParIdTest {
    @Mock
    private SejourRepository sejourRepository;

    private GetSejourParId underTest;

    @BeforeEach
    void setUp() {
        underTest = new GetSejourParId(sejourRepository);
    }

    @Test
    void Tout_se_passe_bien() {
        // GIVEN
        given(sejourRepository.getParIdOuErreur("sejour-id")).willReturn(new SejourSnapshot(
                "sejour-id",
                "reservation-id",
                new BigDecimal(50),
                LocalDateTime.of(2023, 10, 16, 9, 18, 0),
                LocalDateTime.of(2023, 10, 17, 9, 18, 0),
                Optional.empty(),
                Optional.empty()
        ));

        // WHEN
        SejourSnapshot result = underTest.handle("sejour-id");

        // THEN
        assertThat(result).isEqualTo(new SejourSnapshot(
                "sejour-id",
                "reservation-id",
                new BigDecimal(50),
                LocalDateTime.of(2023, 10, 16, 9, 18, 0),
                LocalDateTime.of(2023, 10, 17, 9, 18, 0),
                Optional.empty(),
                Optional.empty()
        ));
    }

    @Nested
    public class InformerQuandIlYAUneErreur {
        @Test
        void La_reservation_n_existe_pas() {
            // GIVEN
            doThrow(new SejourNonTrouveException()).when(sejourRepository).getParIdOuErreur("inconnu");

            // WHEN
            assertThatThrownBy(() -> underTest.handle("inconnu"))
                    // THEN
                    .isInstanceOf(SejourNonTrouveException.class)
                    .hasMessage("Trip is not found");
        }
    }
}
