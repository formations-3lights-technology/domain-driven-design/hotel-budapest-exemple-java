package com.threelightstechnology.hotelbudapest.deroulement_sejour.unit;

import com.threelightstechnology.hotelbudapest.deroulement_sejour.domain.sejour.SejourRepository;
import com.threelightstechnology.hotelbudapest.deroulement_sejour.domain.sejour.SejourSnapshot;
import com.threelightstechnology.hotelbudapest.deroulement_sejour.domain.sejour.events.EnvoiFactureDemandeEvent;
import com.threelightstechnology.hotelbudapest.deroulement_sejour.domain.sejour.events.SejourTermineEvent;
import com.threelightstechnology.hotelbudapest.deroulement_sejour.domain.sejour.exceptions.SejourMontantPayeNonConformeException;
import com.threelightstechnology.hotelbudapest.deroulement_sejour.domain.sejour.exceptions.SejourNonTrouveException;
import com.threelightstechnology.hotelbudapest.deroulement_sejour.use_cases.finir_sejour.commad_handler.FinirUnSejourCommand;
import com.threelightstechnology.hotelbudapest.deroulement_sejour.use_cases.finir_sejour.commad_handler.FinirUnSejourCommandHandler;
import com.threelightstechnology.hotelbudapest.shared.event_publisher.EventPublisher;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.verify;

@ExtendWith(MockitoExtension.class)
public class FinirUnSejourCommandHandlerTest {
    @Mock
    private SejourRepository sejourRepository;
    @Mock
    private EventPublisher eventPublisher;

    private FinirUnSejourCommandHandler underTest;

    @BeforeEach
    void setUp() {
        underTest = new FinirUnSejourCommandHandler(sejourRepository, eventPublisher);
    }

    @Test
    void Tout_se_passe_bien() {
        // GIVEN
        given(sejourRepository.getParIdOuErreur("sejour-id")).willReturn(
                new SejourSnapshot(
                        "sejour-id",
                        "reservation-id",
                        new BigDecimal(50),
                        LocalDateTime.of(2023, 10, 16, 9, 18, 0),
                        LocalDateTime.of(2023, 10, 20, 9, 18, 0),
                        Optional.empty(),
                        Optional.empty()
                )
        );
        FinirUnSejourCommand command = new FinirUnSejourCommand(
                "sejour-id",
                LocalDateTime.of(2023, 10, 19, 9, 18),
                new BigDecimal(200),
                Optional.of("facture@email.com")
        );

        // WHEN
        underTest.handle(command);

        // THEN
        verify(sejourRepository).save(
                new SejourSnapshot(
                        "sejour-id",
                        "reservation-id",
                        new BigDecimal(50),
                        LocalDateTime.of(2023, 10, 16, 9, 18, 0),
                        LocalDateTime.of(2023, 10, 20, 9, 18, 0),
                        Optional.of(LocalDateTime.of(2023, 10, 19, 9, 18)),
                        Optional.of(new BigDecimal(200))
                )
        );
        verify(eventPublisher).publish(List.of(
                new SejourTermineEvent("sejour-id", LocalDateTime.of(2023, 10, 19, 9, 18)),
                new EnvoiFactureDemandeEvent("sejour-id", "reservation-id", Optional.of("facture@email.com"), new BigDecimal(200))
        ));
    }

    @Nested
    public class InformerQuandIlYAUneErreur {
        @Test
        void Le_sejour_n_existe_pas() {
            // GIVEN
            doThrow(new SejourNonTrouveException()).when(sejourRepository).getParIdOuErreur("inconnu");
            FinirUnSejourCommand command = new FinirUnSejourCommand(
                    "inconnu",
                    LocalDateTime.of(2023, 10, 19, 9, 18),
                    new BigDecimal(200),
                    Optional.of("facture@email.com")
            );

            // WHEN
            assertThatThrownBy(() -> underTest.handle(command))
                    // THEN
                    .isInstanceOf(SejourNonTrouveException.class)
                    .hasMessage("Trip is not found");
        }

        @Test
        void Le_montant_paye_ne_correspond_pas() {
            // GIVEN
            given(sejourRepository.getParIdOuErreur("sejour-id")).willReturn(
                    new SejourSnapshot(
                            "sejour-id",
                            "reservation-id",
                            new BigDecimal(50),
                            LocalDateTime.of(2023, 10, 16, 9, 18, 0),
                            LocalDateTime.of(2023, 10, 20, 9, 18, 0),
                            Optional.empty(),
                            Optional.empty()
                    )
            );
            FinirUnSejourCommand command = new FinirUnSejourCommand(
                    "sejour-id",
                    LocalDateTime.of(2023, 10, 19, 9, 18),
                    new BigDecimal(100),
                    Optional.of("facture@email.com")
            );

            // WHEN
            assertThatThrownBy(() -> underTest.handle(command))
                    // THEN
                    .isInstanceOf(SejourMontantPayeNonConformeException.class)
                    .hasMessage("Amount is not good");
        }
    }
}
