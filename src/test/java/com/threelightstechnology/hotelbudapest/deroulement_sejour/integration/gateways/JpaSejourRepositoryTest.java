package com.threelightstechnology.hotelbudapest.deroulement_sejour.integration.gateways;

import com.threelightstechnology.hotelbudapest.deroulement_sejour.domain.sejour.SejourRepository;
import com.threelightstechnology.hotelbudapest.deroulement_sejour.domain.sejour.SejourSnapshot;
import com.threelightstechnology.hotelbudapest.deroulement_sejour.domain.sejour.exceptions.SejourNonTrouveException;
import com.threelightstechnology.hotelbudapest.deroulement_sejour.infrastructure.gateways.sejour_repository.JpaSejourRepository;
import com.threelightstechnology.hotelbudapest.deroulement_sejour.infrastructure.gateways.sejour_repository.SejourDataSource;
import com.threelightstechnology.hotelbudapest.deroulement_sejour.infrastructure.gateways.sejour_repository.SejourEntity;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.ActiveProfiles;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

@DataJpaTest
@ActiveProfiles("test")
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
public class JpaSejourRepositoryTest {
    @Autowired
    private SejourDataSource sejourDataSource;

    private SejourRepository underTest;

    @BeforeEach
    void setUp() {
        underTest = new JpaSejourRepository(sejourDataSource);
    }

    @Nested
    public class NextId {
        @Test
        void Tout_se_passe_bien() {
            // WHEN
            String result = underTest.nextId();

            // THEN
            assertThat(result).isInstanceOf(String.class);
        }
    }

    @Nested
    public class Save {
        @Test
        void Tout_se_passe_bien() {
            // GIVEN
            SejourSnapshot sejour = new SejourSnapshot(
                    "next-id",
                    "reservation-id",
                    new BigDecimal(50),
                    LocalDateTime.of(2023, 10, 16, 9, 18, 0),
                    LocalDateTime.of(2023, 10, 17, 9, 18, 0),
                    Optional.empty(),
                    Optional.empty()
            );

            // WHEN
            underTest.save(sejour);

            // THEN
            List<SejourSnapshot> result = sejourDataSource
                    .findAll()
                    .stream()
                    .map(SejourEntity::mapVersSnapshot)
                    .toList();
            assertThat(result).isEqualTo(List.of(sejour));
        }
    }

    @Nested
    public class Get {
        @Test
        void Tout_se_passe_bien() {
            // GIVEN
            SejourSnapshot sejour = new SejourSnapshot(
                    "sejour-id",
                    "reservation-id",
                    new BigDecimal(50),
                    LocalDateTime.of(2023, 10, 16, 9, 18, 0),
                    LocalDateTime.of(2023, 10, 17, 9, 18, 0),
                    Optional.empty(),
                    Optional.empty()
            );
            underTest.save(sejour);

            // WHEN
            SejourSnapshot result = underTest.getParIdOuErreur("sejour-id");

            // THEN
            assertThat(result).isEqualTo(sejour);
        }

        @Test
        void Non_trouvee() {
            // WHEN
            assertThatThrownBy(() -> underTest.getParIdOuErreur("inconnue"))
                    // THEN
                    .isInstanceOf(SejourNonTrouveException.class)
                    .hasMessage("Trip is not found");
        }
    }

    @Nested
    public class ParDateDeFin {
        @Test
        void Tout_se_passe_bien() {
            // GIVEN
            SejourSnapshot sejourKo = new SejourSnapshot(
                    "sejour-id",
                    "reservation-id",
                    new BigDecimal(50),
                    LocalDateTime.of(2023, 10, 16, 9, 18, 0),
                    LocalDateTime.of(2023, 10, 17, 9, 18, 0),
                    Optional.of(LocalDateTime.of(2023, 10, 17, 9, 18, 0)),
                    Optional.empty()
            );
            SejourSnapshot sejourOk = new SejourSnapshot(
                    "sejour-id-2",
                    "reservation-id-2",
                    new BigDecimal(50),
                    LocalDateTime.of(2023, 10, 16, 9, 18, 0),
                    LocalDateTime.of(2023, 10, 17, 9, 18, 0),
                    Optional.of(LocalDateTime.of(2023, 10, 22, 9, 18, 0)),
                    Optional.of(new BigDecimal(10))
            );
            SejourSnapshot sejourNonFini = new SejourSnapshot(
                    "sejour-id-3",
                    "reservation-id-3",
                    new BigDecimal(50),
                    LocalDateTime.of(2023, 10, 16, 9, 18, 0),
                    LocalDateTime.of(2023, 10, 17, 9, 18, 0),
                    Optional.empty(),
                    Optional.empty()
            );
            underTest.save(sejourKo);
            underTest.save(sejourOk);
            underTest.save(sejourNonFini);

            // WHEN
            List<SejourSnapshot> result = underTest.getParDateDeFin(LocalDate.of(2023, 10, 22));

            // THEN
            assertThat(result).isEqualTo(List.of(sejourOk));
        }

        @Test
        void Non_trouvee() {
            // WHEN
            List<SejourSnapshot> result = underTest.getParDateDeFin(LocalDate.of(2023, 10, 22));

            // THEN
            assertThat(result).isEqualTo(List.of());
        }
    }
}
