package com.threelightstechnology.hotelbudapest.deroulement_sejour.integration.gateways;

import com.threelightstechnology.hotelbudapest.deroulement_sejour.domain.reservation.Reservation;
import com.threelightstechnology.hotelbudapest.deroulement_sejour.domain.reservation.ReservationLoader;
import com.threelightstechnology.hotelbudapest.deroulement_sejour.domain.reservation.ReservationNonTrouveeException;
import com.threelightstechnology.hotelbudapest.deroulement_sejour.infrastructure.gateways.reservation_loader.HttpReservationLoader;
import com.threelightstechnology.hotelbudapest.deroulement_sejour.infrastructure.gateways.reservation_loader.ReservationApiResponse;
import com.threelightstechnology.hotelbudapest.shared.domain.http_client.HttpClient;
import com.threelightstechnology.hotelbudapest.shared.domain.http_client.HttpResponse;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.math.BigDecimal;
import java.time.LocalDateTime;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.doThrow;

@ExtendWith(MockitoExtension.class)
public class HttpReservationLoaderTest {
    @Mock
    private HttpClient httpClient;

    private ReservationLoader underTest;

    @BeforeEach
    void setUp() {
        underTest = new HttpReservationLoader(httpClient);
    }

    @Nested
    public class GetParIdOuErreur {
        @Test
        void Tout_se_passe_bien() {
            // GIVEN
            given(httpClient.get("/booking/rooms/reservation-id")).willReturn(new HttpResponse<>(
                    new ReservationApiResponse(
                            "reservation-id",
                            "reservation@email.com",
                            "11",
                            2,
                            new BigDecimal(50),
                            LocalDateTime.of(2023, 10, 16, 9, 18, 0),
                            LocalDateTime.of(2023, 10, 17, 9, 18, 0)
                    )
            ));

            // WHEN
            Reservation result = underTest.getParIdOuErreur("reservation-id");

            // THEN
            assertThat(result).isEqualTo(new Reservation(
                    "reservation-id",
                    "reservation@email.com",
                    new BigDecimal(50),
                    LocalDateTime.of(2023, 10, 16, 9, 18, 0),
                    LocalDateTime.of(2023, 10, 17, 9, 18, 0)
            ));
        }

        @Test
        void Non_trouve() {
            // GIVEN
            doThrow(new RuntimeException()).when(httpClient).get(anyString());

            // WHEN
            assertThatThrownBy(() -> underTest.getParIdOuErreur("inconnue"))
                    // THEN
                    .isInstanceOf(ReservationNonTrouveeException.class)
                    .hasMessage("Booking is not found");
        }
    }
}
