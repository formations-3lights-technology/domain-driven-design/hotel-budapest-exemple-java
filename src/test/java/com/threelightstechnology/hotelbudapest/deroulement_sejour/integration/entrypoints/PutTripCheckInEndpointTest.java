package com.threelightstechnology.hotelbudapest.deroulement_sejour.integration.entrypoints;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.threelightstechnology.hotelbudapest.deroulement_sejour.use_cases.demarrer_sejour.DemarrerUnSejourCommand;
import com.threelightstechnology.hotelbudapest.shared.command_disptacher.CommandDispatcher;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.*;

import java.time.LocalDateTime;
import java.util.HashMap;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.verify;


@ExtendWith(MockitoExtension.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class PutTripCheckInEndpointTest {
    @Autowired
    private TestRestTemplate testRestTemplate;

    @MockBean
    private CommandDispatcher commandDispatcher;

    @Nested
    @DisplayName("Returns 202")
    class Returns202 {
        @Test
        void tout_est_renseigne() throws JsonProcessingException {
            // GIVEN
            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(MediaType.APPLICATION_JSON);
            var body = new HashMap<>();
            body.put("bookingId", "reservation-id");
            body.put("startDate", "2022-11-16");

            // WHEN
            var response = testRestTemplate.exchange(
                    "/v1/trips/check-in",
                    HttpMethod.PUT,
                    new HttpEntity<>(new ObjectMapper().writeValueAsString(body), headers),
                    Void.class
            );

            // THEN
            verify(commandDispatcher).dispatch(
                    new DemarrerUnSejourCommand(
                            "reservation-id",
                            LocalDateTime.of(2022, 11, 16, 0, 0, 0)
                    )
            );
            assertThat(response.getStatusCode()).isEqualTo(HttpStatus.ACCEPTED);
        }
    }
}
