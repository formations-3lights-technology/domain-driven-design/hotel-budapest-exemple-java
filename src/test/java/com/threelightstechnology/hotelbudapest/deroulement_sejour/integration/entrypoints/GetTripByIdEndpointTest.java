package com.threelightstechnology.hotelbudapest.deroulement_sejour.integration.entrypoints;

import com.threelightstechnology.hotelbudapest.deroulement_sejour.domain.sejour.SejourSnapshot;
import com.threelightstechnology.hotelbudapest.deroulement_sejour.use_cases.GetSejourParId;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Objects;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.BDDMockito.given;

@ExtendWith(MockitoExtension.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class GetTripByIdEndpointTest {
    @Autowired
    private TestRestTemplate testRestTemplate;

    @MockBean
    private GetSejourParId getSejourParId;

    @Nested
    @DisplayName("Returns 200")
    class Returns200 {
        @Test
        void tout_est_ok() {
            // GIVEN
            given(getSejourParId.handle("sejour-id")).willReturn(new SejourSnapshot(
                    "sejour-id",
                    "reservation-id",
                    new BigDecimal(50),
                    LocalDateTime.of(2023, 10, 16, 9, 18, 0),
                    LocalDateTime.of(2023, 10, 17, 9, 18, 0),
                    Optional.empty(),
                    Optional.empty()
            ));

            // WHEN
            var response = testRestTemplate.exchange(
                    "/v1/trips/sejour-id",
                    HttpMethod.GET,
                    null,
                    String.class
            );

            // THEN
            assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
            assertThat(Objects.requireNonNull(response.getBody())).hasToString(
                    "{" +
                            "\"id\":\"sejour-id\"," +
                            "\"priceByNight\":50," +
                            "\"bookingId\":\"reservation-id\"," +
                            "\"checkInDate\":\"2023-10-16T09:18:00\"," +
                            "\"expectedEndDate\":\"2023-10-17T09:18:00\"," +
                            "\"endDate\":null," +
                            "\"amount\":null" +
                            "}"
            );
        }
    }
}
