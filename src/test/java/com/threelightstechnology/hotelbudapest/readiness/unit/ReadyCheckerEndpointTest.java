package com.threelightstechnology.hotelbudapest.readiness.unit;

import com.threelightstechnology.hotelbudapest.readiness.usecases.ReadyChecker;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;


public class ReadyCheckerEndpointTest {
    @Test
    void tout_est_ok() {
        // WHEN
        var result = new ReadyChecker().handle();

        // THEN
        assertThat(result.isUp()).isTrue();
    }
}
