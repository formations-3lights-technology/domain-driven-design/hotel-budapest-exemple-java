package com.threelightstechnology.hotelbudapest.shared.unit.event_publisher;

import com.threelightstechnology.hotelbudapest.shared.event_publisher.DomainEvent;

public class MyEvent2 extends DomainEvent {
    static final String label = MyEvent2.class.getName();

    public String label() {
        return MyEvent2.label;
    }
}
