package com.threelightstechnology.hotelbudapest.shared.unit.event_publisher;

import com.threelightstechnology.hotelbudapest.shared.event_publisher.DomainEvent;

public class MyEvent extends DomainEvent {
    static final String label = MyEvent.class.getName();

    public String label() {
        return MyEvent.label;
    }
}
