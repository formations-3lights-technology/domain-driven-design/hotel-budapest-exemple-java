package com.threelightstechnology.hotelbudapest.shared.unit.event_publisher;

import com.threelightstechnology.hotelbudapest.shared.event_publisher.DomainEvent;
import com.threelightstechnology.hotelbudapest.shared.event_publisher.EventListener;
import com.threelightstechnology.hotelbudapest.shared.event_publisher.EventPublisher;
import com.threelightstechnology.hotelbudapest.shared.event_publisher.InternalEventPublisher;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;
import java.util.Map;
import java.util.Set;

import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.verify;

@ExtendWith(MockitoExtension.class)
public class EventPublisherTest {
    @Mock
    private EventListener<DomainEvent> eventListener1;
    @Mock
    private EventListener<DomainEvent> eventListener2;
    @Mock
    private EventListener<DomainEvent> eventListener3;

    private EventPublisher underTest;

    @BeforeEach
    void setUp() {
        underTest = new InternalEventPublisher();
    }

    @Nested
    class PublierLesEvenementsAuxBonsListeners {
        @Test
        void Tout_se_passe_bien() {
            // GIVEN
            var event = new MyEvent();
            var event2 = new MyEvent2();
            underTest.registerListeners(Map.of(
                    event.label(), Set.of(eventListener1, eventListener2),
                    event2.label(), Set.of(eventListener3)
            ));

            // WHEN
            underTest.publish(List.of(event, event2));

            // THEN
            verify(eventListener1).listen(event);
            verify(eventListener2).listen(event);
            verify(eventListener3).listen(event2);
        }

        @Test
        void Un_listener_fait_une_erreur() {
            // GIVEN
            var event = new MyEvent();
            var event2 = new MyEvent2();
            doThrow(new RuntimeException("Exception")).when(eventListener2).listen(event);
            underTest.registerListeners(Map.of(
                    event.label(), Set.of(eventListener1, eventListener2),
                    event2.label(), Set.of(eventListener3)
            ));

            // WHEN
            underTest.publish(List.of(event, event2));

            // THEN
            verify(eventListener1).listen(event);
            verify(eventListener2).listen(event);
            verify(eventListener3).listen(event2);
        }
    }
}
