package com.threelightstechnology.hotelbudapest.shared.unit.command_dispatcher;

import com.threelightstechnology.hotelbudapest.shared.command_disptacher.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Map;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.mockito.Mockito.verify;

@ExtendWith(MockitoExtension.class)
public class CommandDispatcherTest {
    @Mock
    private CommandHandler<Command> commandHandlerTest1;

    private CommandDispatcher underTest;

    @BeforeEach
    void setUp() {
        underTest = new InternalCommandDispatcher();
    }

    @Test
    void dispatch_des_commandes_enregistrees_dans_le_bus() {
        // GIVEN
        var command = new MyCommand();
        underTest.registerHandlers(Map.of(
                command.label(), commandHandlerTest1
        ));

        // WHEN
        underTest.dispatch(command);

        // THEN
        verify(commandHandlerTest1).handle(command);
    }

    @Test
    void dispatch_une_commande_non_enregistree_dans_le_bus() {
        // GIVEN
        var command = new MyCommand();
        underTest.registerHandlers(Map.of());

        try {
            // WHEN
            underTest.dispatch(command);
            assertThat(true).isFalse();
        } catch (Exception e) {
            // THEN
            assert e instanceof CommandNotRegisteredException;
            assertThat(e.getMessage()).isEqualTo("The dispatched command is not registered");
        }
    }
}
