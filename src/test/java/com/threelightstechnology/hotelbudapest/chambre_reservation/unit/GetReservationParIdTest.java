package com.threelightstechnology.hotelbudapest.chambre_reservation.unit;

import com.threelightstechnology.hotelbudapest.chambre_reservation.domain.reservation.ReservationRepository;
import com.threelightstechnology.hotelbudapest.chambre_reservation.domain.reservation.ReservationSnapshot;
import com.threelightstechnology.hotelbudapest.chambre_reservation.domain.reservation.exceptions.ReservationNonTrouveeException;
import com.threelightstechnology.hotelbudapest.chambre_reservation.use_cases.GetReservationParId;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.math.BigDecimal;
import java.time.LocalDateTime;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.doThrow;

@ExtendWith(MockitoExtension.class)
public class GetReservationParIdTest {
    @Mock
    private ReservationRepository reservationRepository;

    private GetReservationParId underTest;

    @BeforeEach
    void setUp() {
        underTest = new GetReservationParId(reservationRepository);
    }

    @Test
    void Tout_se_passe_bien() {
        // GIVEN
        LocalDateTime dateDeDebut = LocalDateTime.of(2023, 10, 15, 9, 18, 0);
        LocalDateTime dateDeFin = LocalDateTime.of(2023, 10, 16, 9, 18, 0);
        given(reservationRepository.getParIdOuErreur("reservation-id")).willReturn(new ReservationSnapshot(
                "reservation-id",
                "email@reservation.com",
                "11",
                2,
                new BigDecimal(10),
                LocalDateTime.of(2023, 10, 15, 9, 18, 0),
                LocalDateTime.of(2023, 10, 16, 9, 18, 0)
        ));

        // WHEN
        ReservationSnapshot result = underTest.handle("reservation-id");

        // THEN
        assertThat(result).isEqualTo(new ReservationSnapshot(
                "reservation-id",
                "email@reservation.com",
                "11",
                2,
                new BigDecimal(10),
                LocalDateTime.of(2023, 10, 15, 9, 18, 0),
                LocalDateTime.of(2023, 10, 16, 9, 18, 0)
        ));
    }

    @Nested
    public class InformerQuandIlYAUneErreur {
        @Test
        void La_reservation_n_existe_pas() {
            // GIVEN
            doThrow(new ReservationNonTrouveeException()).when(reservationRepository).getParIdOuErreur("inconnue");

            // WHEN
            assertThatThrownBy(() -> underTest.handle("inconnue"))
                    // THEN
                    .isInstanceOf(ReservationNonTrouveeException.class)
                    .hasMessage("Booking is not found");
        }
    }
}
