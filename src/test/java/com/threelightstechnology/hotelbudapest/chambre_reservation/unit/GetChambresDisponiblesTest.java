package com.threelightstechnology.hotelbudapest.chambre_reservation.unit;

import com.threelightstechnology.hotelbudapest.chambre_reservation.domain.chambre.Chambre;
import com.threelightstechnology.hotelbudapest.chambre_reservation.domain.chambre.ChambreLoader;
import com.threelightstechnology.hotelbudapest.chambre_reservation.use_cases.GetChambresDisponibles;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.BDDMockito.given;

@ExtendWith(MockitoExtension.class)
public class GetChambresDisponiblesTest {
    @Mock
    private ChambreLoader chambreLoader;

    private GetChambresDisponibles underTest;

    @BeforeEach
    void setUp() {
        underTest = new GetChambresDisponibles(chambreLoader);
    }

    @Test
    void Tout_se_passe_bien() {
        // GIVEN
        LocalDateTime dateDeDebut = LocalDateTime.of(2023, 10, 15, 9, 18, 0);
        LocalDateTime dateDeFin = LocalDateTime.of(2023, 10, 16, 9, 18, 0);
        given(chambreLoader.getPar(2, dateDeDebut, dateDeFin)).willReturn(List.of(
                new Chambre("11", "nom", 2, new BigDecimal(20), List.of())
        ));

        // WHEN
        List<Chambre> result = underTest.handle(2, dateDeDebut, dateDeFin);

        // THEN
        assertThat(result).isEqualTo(List.of(
                new Chambre("11", "nom", 2, new BigDecimal(20), List.of())
        ));
    }
}
