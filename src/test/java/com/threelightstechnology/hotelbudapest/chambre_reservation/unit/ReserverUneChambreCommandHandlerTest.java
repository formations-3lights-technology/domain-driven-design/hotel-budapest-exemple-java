package com.threelightstechnology.hotelbudapest.chambre_reservation.unit;

import com.threelightstechnology.hotelbudapest.chambre_reservation.domain.chambre.Chambre;
import com.threelightstechnology.hotelbudapest.chambre_reservation.domain.chambre.ChambreLoader;
import com.threelightstechnology.hotelbudapest.chambre_reservation.domain.chambre.ChambreNonTrouveeException;
import com.threelightstechnology.hotelbudapest.chambre_reservation.domain.chambre.ChambreReservation;
import com.threelightstechnology.hotelbudapest.chambre_reservation.domain.reservation.ReservationEffectueeEvent;
import com.threelightstechnology.hotelbudapest.chambre_reservation.domain.reservation.ReservationRepository;
import com.threelightstechnology.hotelbudapest.chambre_reservation.domain.reservation.ReservationSnapshot;
import com.threelightstechnology.hotelbudapest.chambre_reservation.domain.reservation.exceptions.ReservationAuMoinsUneNuitException;
import com.threelightstechnology.hotelbudapest.chambre_reservation.domain.reservation.exceptions.ReservationChambreDejaReserveeException;
import com.threelightstechnology.hotelbudapest.chambre_reservation.domain.reservation.exceptions.ReservationChambreTropPetiteException;
import com.threelightstechnology.hotelbudapest.chambre_reservation.use_cases.reserver_chambre.ReserverUneChambreCommand;
import com.threelightstechnology.hotelbudapest.chambre_reservation.use_cases.reserver_chambre.ReserverUneChambreCommandHandler;
import com.threelightstechnology.hotelbudapest.shared.event_publisher.EventPublisher;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.verify;

@ExtendWith(MockitoExtension.class)
public class ReserverUneChambreCommandHandlerTest {
    @Mock
    private ReservationRepository reservationRepository;
    @Mock
    private ChambreLoader chambreLoader;
    @Mock
    private EventPublisher eventPublisher;

    private ReserverUneChambreCommandHandler underTest;

    @BeforeEach
    void setUp() {
        underTest = new ReserverUneChambreCommandHandler(reservationRepository, chambreLoader, eventPublisher);
    }

    @Test
    void Tout_se_passe_bien() {
        // GIVEN
        given(chambreLoader.getParNumeroOuErreur("11")).willReturn(new Chambre(
                "11",
                "nom",
                2,
                new BigDecimal(10),
                List.of()
        ));
        given(reservationRepository.nextId()).willReturn("next-id");
        ReserverUneChambreCommand command = new ReserverUneChambreCommand(
                "email@reservation.com",
                "11",
                2,
                LocalDateTime.of(2023, 10, 15, 9, 18, 0),
                LocalDateTime.of(2023, 10, 16, 9, 18, 0)
        );

        // WHEN
        underTest.handle(command);

        // THEN
        verify(reservationRepository).save(new ReservationSnapshot(
                "next-id",
                "email@reservation.com",
                "11",
                2,
                new BigDecimal(10),
                LocalDateTime.of(2023, 10, 15, 9, 18, 0),
                LocalDateTime.of(2023, 10, 16, 9, 18, 0)
        ));
        verify(eventPublisher).publish(List.of(
                new ReservationEffectueeEvent(
                        "next-id",
                        "email@reservation.com",
                        "11",
                        2,
                        new BigDecimal(10),
                        LocalDateTime.of(2023, 10, 15, 9, 18, 0),
                        LocalDateTime.of(2023, 10, 16, 9, 18, 0)
                )
        ));
    }

    @Nested
    public class InformerQuandIlYAUneErreur {
        @Test
        void La_chambre_n_existe_pas() {
            // GIVEN
            doThrow(new ChambreNonTrouveeException()).when(chambreLoader).getParNumeroOuErreur("inconnue");
            ReserverUneChambreCommand command = new ReserverUneChambreCommand(
                    "email@reservation.com",
                    "inconnue",
                    2,
                    LocalDateTime.of(2023, 10, 15, 9, 18, 0),
                    LocalDateTime.of(2023, 10, 16, 9, 18, 0)
            );

            // WHEN
            assertThatThrownBy(() -> underTest.handle(command))
                    // THEN
                    .isInstanceOf(ChambreNonTrouveeException.class)
                    .hasMessage("Room is not found");
        }

        @Test
        void Moins_d_une_nuit() {
            // GIVEN
            given(chambreLoader.getParNumeroOuErreur("11")).willReturn(new Chambre(
                    "11",
                    "nom",
                    2,
                    new BigDecimal(10),
                    List.of()
            ));
            ReserverUneChambreCommand command = new ReserverUneChambreCommand(
                    "email@reservation.com",
                    "11",
                    2,
                    LocalDateTime.of(2023, 10, 15, 9, 18, 0),
                    LocalDateTime.of(2023, 10, 15, 9, 18, 0)
            );

            // WHEN
            assertThatThrownBy(() -> underTest.handle(command))
                    // THEN
                    .isInstanceOf(ReservationAuMoinsUneNuitException.class)
                    .hasMessage("The reservation must be at least for one night");
        }
    }

    @Nested
    public class LaChambreNeCorrespondPasALaReservation {
        @BeforeEach
        void setUp() {
            given(chambreLoader.getParNumeroOuErreur("11")).willReturn(new Chambre(
                    "11",
                    "nom",
                    2,
                    new BigDecimal(10),
                    List.of(
                            new ChambreReservation(
                                    LocalDateTime.of(2023, 10, 15, 9, 18, 0),
                                    LocalDateTime.of(2023, 10, 17, 9, 18, 0)
                            )
                    )
            ));
        }

        @Test
        void Elle_est_trop_petite() {
            // GIVEN
            ReserverUneChambreCommand command = new ReserverUneChambreCommand(
                    "email@reservation.com",
                    "11",
                    3,
                    LocalDateTime.of(2023, 10, 15, 9, 18, 0),
                    LocalDateTime.of(2023, 10, 16, 9, 18, 0)
            );

            // WHEN
            assertThatThrownBy(() -> underTest.handle(command))
                    // THEN
                    .isInstanceOf(ReservationChambreTropPetiteException.class)
                    .hasMessage("The capacity of the room is not enough");
        }

        @Test
        void Elle_est_deja_reservee() {
            // GIVEN
            ReserverUneChambreCommand command = new ReserverUneChambreCommand(
                    "email@reservation.com",
                    "11",
                    2,
                    LocalDateTime.of(2023, 10, 14, 9, 18, 0),
                    LocalDateTime.of(2023, 10, 16, 9, 18, 0)
            );

            // WHEN
            assertThatThrownBy(() -> underTest.handle(command))
                    // THEN
                    .isInstanceOf(ReservationChambreDejaReserveeException.class)
                    .hasMessage("Room is already booked");
        }
    }
}
