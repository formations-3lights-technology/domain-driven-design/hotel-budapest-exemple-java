package com.threelightstechnology.hotelbudapest.chambre_reservation.integration.gateways;

import com.threelightstechnology.hotelbudapest.chambre_reservation.domain.reservation.ReservationRepository;
import com.threelightstechnology.hotelbudapest.chambre_reservation.domain.reservation.ReservationSnapshot;
import com.threelightstechnology.hotelbudapest.chambre_reservation.domain.reservation.exceptions.ReservationNonTrouveeException;
import com.threelightstechnology.hotelbudapest.chambre_reservation.infrastructure.gateways.reservation_repository.JpaReservationRepository;
import com.threelightstechnology.hotelbudapest.chambre_reservation.infrastructure.gateways.reservation_repository.ReservationDataSource;
import com.threelightstechnology.hotelbudapest.chambre_reservation.infrastructure.gateways.reservation_repository.ReservationEntity;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.ActiveProfiles;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

@DataJpaTest
@ActiveProfiles("test")
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
public class JpaReservationRepositoryTest {
    @Autowired
    private ReservationDataSource reservationDataSource;

    private ReservationRepository underTest;

    @BeforeEach
    void setUp() {
        underTest = new JpaReservationRepository(reservationDataSource);
    }

    @Nested
    public class NextId {
        @Test
        void Tout_se_passe_bien() {
            // WHEN
            String result = underTest.nextId();

            // THEN
            assertThat(result).isInstanceOf(String.class);
        }
    }

    @Nested
    public class Save {
        @Test
        void Tout_se_passe_bien() {
            // GIVEN
            ReservationSnapshot reservation = new ReservationSnapshot(
                    "reservation-id",
                    "email@reservation.com",
                    "11",
                    2,
                    new BigDecimal(10),
                    LocalDateTime.of(2023, 10, 15, 9, 18, 0),
                    LocalDateTime.of(2023, 10, 16, 9, 18, 0)
            );

            // WHEN
            underTest.save(reservation);

            // THEN
            List<ReservationSnapshot> result = reservationDataSource
                    .findAll()
                    .stream()
                    .map(ReservationEntity::mapVersSnapshot)
                    .toList();
            assertThat(result).isEqualTo(List.of(reservation));
        }
    }

    @Nested
    public class Get {
        @Test
        void Tout_se_passe_bien() {
            // GIVEN
            ReservationSnapshot reservation = new ReservationSnapshot(
                    "reservation-id",
                    "email@reservation.com",
                    "11",
                    2,
                    new BigDecimal(10),
                    LocalDateTime.of(2023, 10, 15, 9, 18, 0),
                    LocalDateTime.of(2023, 10, 16, 9, 18, 0)
            );
            underTest.save(reservation);

            // WHEN
            ReservationSnapshot result = underTest.getParIdOuErreur("reservation-id");

            // THEN
            assertThat(result).isEqualTo(reservation);
        }

        @Test
        void Non_trouvee() {
            // WHEN
            assertThatThrownBy(() -> underTest.getParIdOuErreur("inconnue"))
                    // THEN
                    .isInstanceOf(ReservationNonTrouveeException.class)
                    .hasMessage("Booking is not found");
        }
    }
}
