package com.threelightstechnology.hotelbudapest.chambre_reservation.integration.entrypoints;

import com.threelightstechnology.hotelbudapest.chambre_reservation.domain.chambre.Chambre;
import com.threelightstechnology.hotelbudapest.chambre_reservation.use_cases.GetChambresDisponibles;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;
import java.util.Objects;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.BDDMockito.given;

@ExtendWith(MockitoExtension.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class GetAvailableRoomsEndpointTest {
    @Autowired
    private TestRestTemplate testRestTemplate;

    @MockBean
    private GetChambresDisponibles getChambresDisponibles;

    @Nested
    @DisplayName("Returns 200")
    class Returns200 {
        @Test
        void tout_est_ok() {
            // GIVEN
            given(getChambresDisponibles.handle(
                    3,
                    LocalDate.of(2022, 11, 9).atStartOfDay(),
                    LocalDate.of(2022, 11, 12).atStartOfDay()
            )).willReturn(List.of(
                    new Chambre("11", "nom", 2, new BigDecimal(20), List.of())
            ));

            // WHEN
            var response = testRestTemplate.exchange(
                    "/v1/search/rooms/2022-11-09/2022-11-12/3",
                    HttpMethod.GET,
                    null,
                    String.class
            );

            // THEN
            assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
            assertThat(Objects.requireNonNull(response.getBody())).hasToString(
                    "[{" +
                            "\"capacity\":2," +
                            "\"name\":\"nom\"," +
                            "\"number\":\"11\"," +
                            "\"priceByNight\":20" +
                            "}]"
            );
        }
    }
}
