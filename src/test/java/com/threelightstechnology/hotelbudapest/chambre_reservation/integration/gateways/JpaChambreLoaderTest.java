package com.threelightstechnology.hotelbudapest.chambre_reservation.integration.gateways;

import com.threelightstechnology.hotelbudapest.chambre_reservation.domain.chambre.Chambre;
import com.threelightstechnology.hotelbudapest.chambre_reservation.domain.chambre.ChambreLoader;
import com.threelightstechnology.hotelbudapest.chambre_reservation.domain.chambre.ChambreNonTrouveeException;
import com.threelightstechnology.hotelbudapest.chambre_reservation.domain.chambre.ChambreReservation;
import com.threelightstechnology.hotelbudapest.chambre_reservation.domain.reservation.ReservationSnapshot;
import com.threelightstechnology.hotelbudapest.chambre_reservation.infrastructure.gateways.chambre_loader.ChambreDataSource;
import com.threelightstechnology.hotelbudapest.chambre_reservation.infrastructure.gateways.chambre_loader.JpaChambreLoader;
import com.threelightstechnology.hotelbudapest.chambre_reservation.infrastructure.gateways.reservation_repository.ReservationEntity;
import jakarta.persistence.EntityManager;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.ActiveProfiles;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

@DataJpaTest
@ActiveProfiles("test")
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
public class JpaChambreLoaderTest {
    @Autowired
    private ChambreDataSource chambreDataSource;
    @Autowired
    private EntityManager entityManager;

    private ChambreLoader underTest;

    @BeforeEach
    void setUp() {
        underTest = new JpaChambreLoader(chambreDataSource);
    }

    @Nested
    public class RecupererLaChambre {
        @Test
        void Tout_se_passe_bien() {
            // GIVEN
            entityManager.persist(ReservationEntity.mapDepuisSnapshot(new ReservationSnapshot("reservation-id", "email@reservation.com", "11", 2, new BigDecimal(10), LocalDateTime.of(2023, 10, 15, 9, 18, 0), LocalDateTime.of(2023, 10, 16, 9, 18, 0))));
            entityManager.flush();

            // WHEN
            Chambre result = underTest.getParNumeroOuErreur("11");

            // THEN
            assertThat(result).isEqualTo(new Chambre("11", "Chambre Standard", 2, new BigDecimal(80), List.of(new ChambreReservation(LocalDateTime.of(2023, 10, 15, 9, 18, 0), LocalDateTime.of(2023, 10, 16, 9, 18, 0)))));
        }

        @Test
        void Elle_n_existe_pas() {
            // WHEN
            assertThatThrownBy(() -> underTest.getParNumeroOuErreur("inconnue"))
                    // THEN
                    .isInstanceOf(ChambreNonTrouveeException.class).hasMessage("Room is not found");
        }
    }

    @Nested
    public class RecupererLaListeDesChambresCorrespondantes {
        @Test
        void Tout_se_passe_bien() {
            // GIVEN
            entityManager.persist(ReservationEntity.mapDepuisSnapshot(
                    new ReservationSnapshot(
                            "reservation-id-11",
                            "email@reservation.com",
                            "11",
                            1,
                            new BigDecimal(10),
                            LocalDateTime.of(2023, 10, 15, 19, 9, 0),
                            LocalDateTime.of(2023, 10, 16, 19, 9, 0)
                    )
            ));
            entityManager.persist(ReservationEntity.mapDepuisSnapshot(
                    new ReservationSnapshot(
                            "reservation-id-21-1",
                            "email@reservation.com",
                            "21",
                            1,
                            new BigDecimal(10),
                            LocalDateTime.of(2023, 10, 10, 19, 9, 0),
                            LocalDateTime.of(2023, 10, 14, 10, 9, 0)
                    )
            ));
            entityManager.persist(ReservationEntity.mapDepuisSnapshot(
                    new ReservationSnapshot(
                            "reservation-id-31-1",
                            "email@reservation.com",
                            "31",
                            1,
                            new BigDecimal(10),
                            LocalDateTime.of(2023, 10, 17, 19, 9, 0),
                            LocalDateTime.of(2023, 10, 19, 10, 9, 0)
                    )
            ));
            entityManager.flush();

            // WHEN
            List<Chambre> result = underTest.getPar(
                    2,
                    LocalDateTime.of(2023, 10, 15, 0, 9, 0),
                    LocalDateTime.of(2023, 10, 17, 0, 9, 0)
            );

            // THEN
            assertThat(result).isEqualTo(List.of(
                    new Chambre(
                            "12",
                            "Chambre Familiale",
                            4,
                            new BigDecimal(150),
                            List.of()
                    ),
                    new Chambre(
                            "14",
                            "Chambre vue Jardin",
                            3,
                            new BigDecimal(170),
                            List.of()),
                    new Chambre(
                            "21",
                            "Chambre Standard",
                            2,
                            new BigDecimal(80),
                            List.of(
                                    new ChambreReservation(
                                            LocalDateTime.of(2023, 10, 10, 19, 9, 0),
                                            LocalDateTime.of(2023, 10, 14, 10, 9, 0)
                                    )
                            )
                    ),
                    new Chambre(
                            "22",
                            "Suite Familiale",
                            5,
                            new BigDecimal(285),
                            List.of()
                    ),
                    new Chambre(
                            "31",
                            "Chambre Standard",
                            2,
                            new BigDecimal(80),
                            List.of(
                                    new ChambreReservation(
                                            LocalDateTime.of(2023, 10, 17, 19, 9, 0),
                                            LocalDateTime.of(2023, 10, 19, 10, 9, 0)
                                    )
                            )
                    ),
                    new Chambre(
                            "32",
                            "Chambre Deluxe vue Jardin",
                            3,
                            new BigDecimal(235),
                            List.of()
                    )
            ));
        }
    }
}
