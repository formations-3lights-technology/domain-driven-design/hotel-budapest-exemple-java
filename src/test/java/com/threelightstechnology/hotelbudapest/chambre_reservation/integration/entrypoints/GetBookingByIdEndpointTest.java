package com.threelightstechnology.hotelbudapest.chambre_reservation.integration.entrypoints;

import com.threelightstechnology.hotelbudapest.chambre_reservation.domain.reservation.ReservationSnapshot;
import com.threelightstechnology.hotelbudapest.chambre_reservation.use_cases.GetReservationParId;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Objects;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.BDDMockito.given;

@ExtendWith(MockitoExtension.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class GetBookingByIdEndpointTest {
    @Autowired
    private TestRestTemplate testRestTemplate;

    @MockBean
    private GetReservationParId getReservationParId;

    @Nested
    @DisplayName("Returns 200")
    class Returns200 {
        @Test
        void tout_est_ok() {
            // GIVEN
            given(getReservationParId.handle("3")).willReturn(new ReservationSnapshot(
                    "reservation-id",
                    "email@reservation.com",
                    "11",
                    2,
                    new BigDecimal(10),
                    LocalDateTime.of(2023, 10, 15, 9, 18, 0),
                    LocalDateTime.of(2023, 10, 16, 9, 18, 0)
            ));

            // WHEN
            var response = testRestTemplate.exchange(
                    "/v1/booking/rooms/3",
                    HttpMethod.GET,
                    null,
                    String.class
            );

            // THEN
            assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
            assertThat(Objects.requireNonNull(response.getBody())).hasToString(
                    "{" +
                            "\"id\":\"reservation-id\"," +
                            "\"bookingEmail\":\"email@reservation.com\"," +
                            "\"roomNumber\":\"11\"," +
                            "\"totalPerson\":2," +
                            "\"priceByNight\":10," +
                            "\"startDate\":\"2023-10-15T09:18:00\"," +
                            "\"endDate\":\"2023-10-16T09:18:00\"" +
                            "}"
            );
        }
    }
}
