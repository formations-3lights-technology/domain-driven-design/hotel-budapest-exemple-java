package com.threelightstechnology.hotelbudapest.facturation.integration;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.threelightstechnology.hotelbudapest.facturation.use_cases.EnvoyerUneFactureCommand;
import com.threelightstechnology.hotelbudapest.shared.command_disptacher.CommandDispatcher;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.*;

import java.math.BigDecimal;
import java.util.HashMap;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.verify;


@ExtendWith(MockitoExtension.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class PostSendInvoiceEndpointTest {
    @Autowired
    private TestRestTemplate testRestTemplate;

    @MockBean
    private CommandDispatcher commandDispatcher;

    @Nested
    @DisplayName("Returns 202")
    class Returns202 {
        @Test
        void tout_est_renseigne() throws JsonProcessingException {
            // GIVEN
            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(MediaType.APPLICATION_JSON);
            var body = new HashMap<>();
            body.put("invoiceEmail", "facturation@email.com");
            body.put("amount", 200);

            // WHEN
            var response = testRestTemplate.exchange("/v1/invoices/send", HttpMethod.POST, new HttpEntity<>(new ObjectMapper().writeValueAsString(body), headers), Void.class);

            // THEN
            verify(commandDispatcher).dispatch(new EnvoyerUneFactureCommand("facturation@email.com", new BigDecimal(200)));
            assertThat(response.getStatusCode()).isEqualTo(HttpStatus.ACCEPTED);
        }
    }
}
