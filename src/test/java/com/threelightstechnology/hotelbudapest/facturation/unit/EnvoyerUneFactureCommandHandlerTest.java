package com.threelightstechnology.hotelbudapest.facturation.unit;

import com.threelightstechnology.hotelbudapest.facturation.domain.Mail;
import com.threelightstechnology.hotelbudapest.facturation.domain.MailSender;
import com.threelightstechnology.hotelbudapest.facturation.use_cases.EnvoyerUneFactureCommand;
import com.threelightstechnology.hotelbudapest.facturation.use_cases.EnvoyerUneFactureCommandHandler;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.math.BigDecimal;

import static org.mockito.Mockito.doNothing;

@ExtendWith(MockitoExtension.class)
public class EnvoyerUneFactureCommandHandlerTest {
    @Mock
    private MailSender mailSender;

    private EnvoyerUneFactureCommandHandler underTest;

    @BeforeEach
    void setUp() {
        underTest = new EnvoyerUneFactureCommandHandler(mailSender);
    }

    @Test
    void Tout_se_passe_bien() {
        // GIVEN
        var command = new EnvoyerUneFactureCommand("facturation@email.com", new BigDecimal(200));
        doNothing().when(mailSender).send(new Mail(
                "service-facturation@email.com",
                "facturation@email.com",
                "Facture d'un montant de : 200 EUR"
        ));

        // WHEN
        underTest.handle(command);
    }
}
