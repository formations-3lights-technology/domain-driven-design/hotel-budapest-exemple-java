package com.threelightstechnology.hotelbudapest.chambre_reservation.use_cases;

import com.threelightstechnology.hotelbudapest.chambre_reservation.domain.reservation.ReservationRepository;
import com.threelightstechnology.hotelbudapest.chambre_reservation.domain.reservation.ReservationSnapshot;
import org.springframework.stereotype.Service;

@Service
public class GetReservationParId {
    private final ReservationRepository reservationRepository;

    public GetReservationParId(ReservationRepository reservationRepository) {
        this.reservationRepository = reservationRepository;
    }

    public ReservationSnapshot handle(String id) {
        return reservationRepository.getParIdOuErreur(id);
    }
}
