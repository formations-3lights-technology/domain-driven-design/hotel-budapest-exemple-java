package com.threelightstechnology.hotelbudapest.chambre_reservation.use_cases;

import com.threelightstechnology.hotelbudapest.chambre_reservation.domain.chambre.Chambre;
import com.threelightstechnology.hotelbudapest.chambre_reservation.domain.chambre.ChambreLoader;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;

@Service
public class GetChambresDisponibles {
    private final ChambreLoader chambreLoader;

    public GetChambresDisponibles(ChambreLoader chambreLoader) {
        this.chambreLoader = chambreLoader;
    }

    public List<Chambre> handle(int capacite, LocalDateTime dateDeDebut, LocalDateTime dateDeFin) {
        return chambreLoader.getPar(capacite, dateDeDebut, dateDeFin);
    }
}
