package com.threelightstechnology.hotelbudapest.chambre_reservation.use_cases.reserver_chambre;

import com.threelightstechnology.hotelbudapest.shared.command_disptacher.Command;

import java.time.LocalDateTime;
import java.util.Objects;

public class ReserverUneChambreCommand extends Command {
    public static final String label = "BookRoomCommand";
    private final String mailDeReservation;
    private final String numeroDeLaChambre;
    private final int nombreDePersonnes;
    private final LocalDateTime dateDeDebut;
    private final LocalDateTime dateDeFin;

    public ReserverUneChambreCommand(
            String mailDeReservation,
            String numeroDeLaChambre,
            int nombreDePersonnes,
            LocalDateTime dateDeDebut,
            LocalDateTime dateDeFin
    ) {
        super();
        this.mailDeReservation = mailDeReservation;
        this.numeroDeLaChambre = numeroDeLaChambre;
        this.nombreDePersonnes = nombreDePersonnes;
        this.dateDeDebut = dateDeDebut;
        this.dateDeFin = dateDeFin;
    }

    public String label() {
        return ReserverUneChambreCommand.label;
    }

    public String getMailDeReservation() {
        return mailDeReservation;
    }

    public String getNumeroDeLaChambre() {
        return numeroDeLaChambre;
    }

    public int getNombreDePersonnes() {
        return nombreDePersonnes;
    }

    public LocalDateTime getDateDeDebut() {
        return dateDeDebut;
    }

    public LocalDateTime getDateDeFin() {
        return dateDeFin;
    }

    public boolean equals(Object o) {
        if (this==o) return true;
        if (o==null || getClass()!=o.getClass()) return false;
        ReserverUneChambreCommand that = (ReserverUneChambreCommand) o;
        return nombreDePersonnes==that.nombreDePersonnes && Objects.equals(mailDeReservation, that.mailDeReservation) && Objects.equals(numeroDeLaChambre, that.numeroDeLaChambre) && Objects.equals(dateDeDebut, that.dateDeDebut) && Objects.equals(dateDeFin, that.dateDeFin);
    }
}
