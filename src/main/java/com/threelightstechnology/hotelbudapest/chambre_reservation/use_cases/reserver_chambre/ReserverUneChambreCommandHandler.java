package com.threelightstechnology.hotelbudapest.chambre_reservation.use_cases.reserver_chambre;

import com.threelightstechnology.hotelbudapest.chambre_reservation.domain.chambre.ChambreLoader;
import com.threelightstechnology.hotelbudapest.chambre_reservation.domain.reservation.Reservation;
import com.threelightstechnology.hotelbudapest.chambre_reservation.domain.reservation.ReservationRepository;
import com.threelightstechnology.hotelbudapest.chambre_reservation.domain.reservation.ReservationService;
import com.threelightstechnology.hotelbudapest.shared.command_disptacher.CommandHandler;
import com.threelightstechnology.hotelbudapest.shared.event_publisher.EventPublisher;

public class ReserverUneChambreCommandHandler implements CommandHandler<ReserverUneChambreCommand> {
    private final ReservationRepository reservationRepository;
    private final ChambreLoader chambreLoader;
    private final EventPublisher eventPublisher;

    public ReserverUneChambreCommandHandler(
            ReservationRepository reservationRepository,
            ChambreLoader chambreLoader,
            EventPublisher eventPublisher
    ) {
        this.reservationRepository = reservationRepository;
        this.chambreLoader = chambreLoader;
        this.eventPublisher = eventPublisher;
    }

    public void handle(ReserverUneChambreCommand command) {
        Reservation reservation = new ReservationService(reservationRepository, chambreLoader)
                .reserverUneChambre(
                        command.getMailDeReservation(),
                        command.getNumeroDeLaChambre(),
                        command.getNombreDePersonnes(),
                        command.getDateDeDebut(),
                        command.getDateDeFin()
                );

        reservationRepository.save(reservation.getSnapshot());
        eventPublisher.publish(reservation.getRaisedEvents());
    }
}
