package com.threelightstechnology.hotelbudapest.chambre_reservation.domain.chambre;

import com.threelightstechnology.hotelbudapest.shared.domain.erreurs.ElementNonTrouve;

public class ChambreNonTrouveeException extends ElementNonTrouve {
    public ChambreNonTrouveeException() {
        super("Room is not found");
    }
}
