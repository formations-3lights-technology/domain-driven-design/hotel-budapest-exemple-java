package com.threelightstechnology.hotelbudapest.chambre_reservation.domain.reservation.exceptions;

import com.threelightstechnology.hotelbudapest.shared.domain.erreurs.ErreurDeValidation;

public class ReservationChambreTropPetiteException extends ErreurDeValidation {
    public ReservationChambreTropPetiteException() {
        super("The capacity of the room is not enough");
    }
}
