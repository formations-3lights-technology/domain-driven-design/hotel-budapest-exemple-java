package com.threelightstechnology.hotelbudapest.chambre_reservation.domain.reservation.exceptions;

import com.threelightstechnology.hotelbudapest.shared.domain.erreurs.ErreurDeValidation;

public class ReservationChambreDejaReserveeException extends ErreurDeValidation {
    public ReservationChambreDejaReserveeException() {
        super("Room is already booked");
    }
}
