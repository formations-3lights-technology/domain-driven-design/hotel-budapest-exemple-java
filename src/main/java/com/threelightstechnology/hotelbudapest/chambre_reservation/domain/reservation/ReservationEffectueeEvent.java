package com.threelightstechnology.hotelbudapest.chambre_reservation.domain.reservation;

import com.threelightstechnology.hotelbudapest.shared.event_publisher.DomainEvent;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Objects;

public class ReservationEffectueeEvent extends DomainEvent {
    static final String label = "BookingDoneEvent";
    private final String id;
    private final String mailDeReservation;
    private final String numeroDeLaChambre;
    private final int nombreDePersonnes;
    private final BigDecimal prixParNuit;
    private final LocalDateTime dateDeDebut;
    private final LocalDateTime dateDeFin;

    public ReservationEffectueeEvent(
            String id,
            String mailDeReservation,
            String numeroDeLaChambre,
            int nombreDePersonnes,
            BigDecimal prixParNuit,
            LocalDateTime dateDeDebut,
            LocalDateTime dateDeFin
    ) {
        this.id = id;
        this.mailDeReservation = mailDeReservation;
        this.numeroDeLaChambre = numeroDeLaChambre;
        this.nombreDePersonnes = nombreDePersonnes;
        this.prixParNuit = prixParNuit;
        this.dateDeDebut = dateDeDebut;
        this.dateDeFin = dateDeFin;
    }

    public String label() {
        return ReservationEffectueeEvent.label;
    }

    public boolean equals(Object o) {
        if (this==o) return true;
        if (o==null || getClass()!=o.getClass()) return false;
        ReservationEffectueeEvent that = (ReservationEffectueeEvent) o;
        return nombreDePersonnes==that.nombreDePersonnes && Objects.equals(id, that.id) && Objects.equals(mailDeReservation, that.mailDeReservation) && Objects.equals(numeroDeLaChambre, that.numeroDeLaChambre) && Objects.equals(prixParNuit, that.prixParNuit) && Objects.equals(dateDeDebut, that.dateDeDebut) && Objects.equals(dateDeFin, that.dateDeFin);
    }
}
