package com.threelightstechnology.hotelbudapest.chambre_reservation.domain.reservation;

import com.threelightstechnology.hotelbudapest.chambre_reservation.domain.chambre.Chambre;
import com.threelightstechnology.hotelbudapest.chambre_reservation.domain.chambre.ChambreLoader;
import com.threelightstechnology.hotelbudapest.chambre_reservation.domain.reservation.exceptions.ReservationChambreDejaReserveeException;
import com.threelightstechnology.hotelbudapest.chambre_reservation.domain.reservation.exceptions.ReservationChambreTropPetiteException;

import java.time.LocalDateTime;

public class ReservationService {
    private final ReservationRepository reservationRepository;
    private final ChambreLoader chambreLoader;

    public ReservationService(ReservationRepository reservationRepository, ChambreLoader chambreLoader) {
        this.reservationRepository = reservationRepository;
        this.chambreLoader = chambreLoader;
    }

    public Reservation reserverUneChambre(
            String mailDeReservation,
            String numeroDeLaChambre,
            int nombreDePersonnes,
            LocalDateTime dateDeDebut,
            LocalDateTime dateDeFin
    ) {
        Chambre chambre = chambreLoader.getParNumeroOuErreur(numeroDeLaChambre);
        verifierLaChambre(chambre, nombreDePersonnes, dateDeDebut, dateDeFin);

        String reservationId = reservationRepository.nextId();
        return Reservation.effectueePour(
                reservationId,
                mailDeReservation,
                numeroDeLaChambre,
                nombreDePersonnes,
                chambre.prixParNuit(),
                dateDeDebut,
                dateDeFin
        );
    }

    private void verifierLaChambre(
            Chambre chambre,
            int nombreDePersonnes,
            LocalDateTime dateDeDebut,
            LocalDateTime dateDeFin
    ) {
        if (chambre.capacite() < nombreDePersonnes) {
            throw new ReservationChambreTropPetiteException();
        }

        boolean estReservee = chambre.reservations().stream().anyMatch(r -> isDateRangeOverlap(r.dateDeDebut(), r.dateDeFin(), dateDeDebut, dateDeFin));
        if (estReservee) {
            throw new ReservationChambreDejaReserveeException();
        }
    }

    private boolean isDateRangeOverlap(LocalDateTime start1, LocalDateTime end1, LocalDateTime start2, LocalDateTime end2) {
        return !end1.isBefore(start2) && !end2.isBefore(start1);
    }
}
