package com.threelightstechnology.hotelbudapest.chambre_reservation.domain.reservation.exceptions;

import com.threelightstechnology.hotelbudapest.shared.domain.erreurs.ErreurDeValidation;

public class ReservationAuMoinsUneNuitException extends ErreurDeValidation {
    public ReservationAuMoinsUneNuitException() {
        super("The reservation must be at least for one night");
    }
}
