package com.threelightstechnology.hotelbudapest.chambre_reservation.domain.reservation;

import com.threelightstechnology.hotelbudapest.shared.domain.Aggregate;

import java.math.BigDecimal;
import java.time.LocalDateTime;

public class Reservation extends Aggregate {
    private final String id;
    private final String mailDeReservation;
    private final String numeroDeLaChambre;
    private final int nombreDePersonnes;
    private final BigDecimal prixParNuit;
    private final ReservationIntervalle reservationIntervalle;

    private Reservation(
            String id,
            String mailDeReservation,
            String numeroDeLaChambre,
            int nombreDePersonnes,
            BigDecimal prixParNuit,
            ReservationIntervalle reservationIntervalle
    ) {
        super();
        this.id = id;
        this.mailDeReservation = mailDeReservation;
        this.numeroDeLaChambre = numeroDeLaChambre;
        this.nombreDePersonnes = nombreDePersonnes;
        this.prixParNuit = prixParNuit;
        this.reservationIntervalle = reservationIntervalle;
    }

    public static Reservation effectueePour(
            String id,
            String mailDeReservation,
            String numeroDeLaChambre,
            int nombreDePersonnes,
            BigDecimal prixParNuit,
            LocalDateTime dateDeDebut,
            LocalDateTime dateDeFin
    ) {
        ReservationIntervalle reservationIntervalle = ReservationIntervalle.init(dateDeDebut, dateDeFin);
        Reservation reservation = new Reservation(
                id,
                mailDeReservation,
                numeroDeLaChambre,
                nombreDePersonnes,
                prixParNuit,
                reservationIntervalle
        );

        reservation.apply(
                new ReservationEffectueeEvent(
                        id,
                        mailDeReservation,
                        numeroDeLaChambre,
                        nombreDePersonnes,
                        prixParNuit,
                        dateDeDebut,
                        dateDeFin
                )
        );

        return reservation;
    }

    public ReservationSnapshot getSnapshot() {
        return new ReservationSnapshot(
                id,
                mailDeReservation,
                numeroDeLaChambre,
                nombreDePersonnes,
                prixParNuit,
                reservationIntervalle.getDateDeDebut(),
                reservationIntervalle.getDateDeFin()
        );
    }
}
