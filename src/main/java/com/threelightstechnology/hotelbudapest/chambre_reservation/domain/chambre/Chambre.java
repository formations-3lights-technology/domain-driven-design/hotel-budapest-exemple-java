package com.threelightstechnology.hotelbudapest.chambre_reservation.domain.chambre;

import java.math.BigDecimal;
import java.util.List;

public record Chambre(
        String numero,
        String nom,
        int capacite,
        BigDecimal prixParNuit,
        List<ChambreReservation> reservations
) {
}
