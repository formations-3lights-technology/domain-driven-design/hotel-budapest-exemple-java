package com.threelightstechnology.hotelbudapest.chambre_reservation.domain.reservation;

import java.math.BigDecimal;
import java.time.LocalDateTime;

public record ReservationSnapshot(
        String id,
        String mailDeReservation,
        String numeroDeLaChambre,
        int nombreDePersonnes,
        BigDecimal prixParNuit,
        LocalDateTime dateDeDebut,
        LocalDateTime dateDeFin
) {
}
