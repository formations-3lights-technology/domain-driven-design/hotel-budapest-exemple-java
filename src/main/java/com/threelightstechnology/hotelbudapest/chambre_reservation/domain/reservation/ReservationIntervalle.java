package com.threelightstechnology.hotelbudapest.chambre_reservation.domain.reservation;

import com.threelightstechnology.hotelbudapest.chambre_reservation.domain.reservation.exceptions.ReservationAuMoinsUneNuitException;

import java.time.LocalDateTime;

public class ReservationIntervalle {
    private final LocalDateTime dateDeDebut;
    private final LocalDateTime dateDeFin;

    private ReservationIntervalle(LocalDateTime dateDeDebut, LocalDateTime dateDeFin) {
        this.dateDeDebut = dateDeDebut;
        this.dateDeFin = dateDeFin;
    }

    public static ReservationIntervalle init(LocalDateTime dateDeDebut, LocalDateTime dateDeFin) {
        if (dateDeDebut.equals(dateDeFin)) {
            throw new ReservationAuMoinsUneNuitException();
        }
        return new ReservationIntervalle(dateDeDebut, dateDeFin);
    }

    public LocalDateTime getDateDeDebut() {
        return dateDeDebut;
    }

    public LocalDateTime getDateDeFin() {
        return dateDeFin;
    }
}
