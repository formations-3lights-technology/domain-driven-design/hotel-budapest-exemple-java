package com.threelightstechnology.hotelbudapest.chambre_reservation.domain.chambre;

import java.time.LocalDateTime;
import java.util.List;

public interface ChambreLoader {
    List<Chambre> getPar(int capacite, LocalDateTime dateDeDebut, LocalDateTime dateDeFin);

    Chambre getParNumeroOuErreur(String numero);
}
