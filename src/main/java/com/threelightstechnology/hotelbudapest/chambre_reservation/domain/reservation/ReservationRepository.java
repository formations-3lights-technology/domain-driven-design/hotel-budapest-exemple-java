package com.threelightstechnology.hotelbudapest.chambre_reservation.domain.reservation;

public interface ReservationRepository {
    String nextId();

    void save(ReservationSnapshot reservation);

    ReservationSnapshot getParIdOuErreur(String id);
}
