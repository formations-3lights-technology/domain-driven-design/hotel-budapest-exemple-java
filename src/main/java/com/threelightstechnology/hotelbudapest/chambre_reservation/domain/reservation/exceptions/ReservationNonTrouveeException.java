package com.threelightstechnology.hotelbudapest.chambre_reservation.domain.reservation.exceptions;

import com.threelightstechnology.hotelbudapest.shared.domain.erreurs.ElementNonTrouve;

public class ReservationNonTrouveeException extends ElementNonTrouve {
    public ReservationNonTrouveeException() {
        super("Booking is not found");
    }
}
