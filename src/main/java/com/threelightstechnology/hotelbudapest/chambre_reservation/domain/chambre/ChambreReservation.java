package com.threelightstechnology.hotelbudapest.chambre_reservation.domain.chambre;

import java.time.LocalDateTime;

public record ChambreReservation(LocalDateTime dateDeDebut, LocalDateTime dateDeFin) {
}
