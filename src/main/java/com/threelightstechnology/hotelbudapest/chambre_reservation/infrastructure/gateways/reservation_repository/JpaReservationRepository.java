package com.threelightstechnology.hotelbudapest.chambre_reservation.infrastructure.gateways.reservation_repository;

import com.threelightstechnology.hotelbudapest.chambre_reservation.domain.reservation.ReservationRepository;
import com.threelightstechnology.hotelbudapest.chambre_reservation.domain.reservation.ReservationSnapshot;
import com.threelightstechnology.hotelbudapest.chambre_reservation.domain.reservation.exceptions.ReservationNonTrouveeException;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository("chambre_reservation-reservation_repository")
public class JpaReservationRepository implements ReservationRepository {
    private final ReservationDataSource reservationDataSource;

    public JpaReservationRepository(ReservationDataSource reservationDataSource) {
        this.reservationDataSource = reservationDataSource;
    }

    public String nextId() {
        return UUID.randomUUID().toString();
    }

    public void save(ReservationSnapshot reservation) {
        reservationDataSource.save(ReservationEntity.mapDepuisSnapshot(reservation));
    }

    public ReservationSnapshot getParIdOuErreur(String id) {
        return reservationDataSource.findByIdIs(id)
                .map(ReservationEntity::mapVersSnapshot)
                .orElseThrow(ReservationNonTrouveeException::new);
    }
}
