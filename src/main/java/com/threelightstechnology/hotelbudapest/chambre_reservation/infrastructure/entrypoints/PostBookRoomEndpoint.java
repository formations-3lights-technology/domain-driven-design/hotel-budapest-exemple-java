package com.threelightstechnology.hotelbudapest.chambre_reservation.infrastructure.entrypoints;

import com.threelightstechnology.hotelbudapest.chambre_reservation.use_cases.reserver_chambre.ReserverUneChambreCommand;
import com.threelightstechnology.hotelbudapest.shared.command_disptacher.CommandDispatcher;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDate;

@RestController
public class PostBookRoomEndpoint {
    private final CommandDispatcher commandDispatcher;

    public PostBookRoomEndpoint(CommandDispatcher commandDispatcher) {
        this.commandDispatcher = commandDispatcher;
    }

    @PostMapping(value = "/v1/booking/rooms")
    @ResponseStatus(HttpStatus.ACCEPTED)
    void execute(@RequestBody Body body) {
        commandDispatcher.dispatch(new ReserverUneChambreCommand(
                body.bookingEmail(),
                body.roomNumber(),
                body.totalPerson(),
                body.startDate().atStartOfDay(),
                body.endDate().atStartOfDay()
        ));
    }

    private record Body(
            String bookingEmail,
            String roomNumber,
            int totalPerson,
            LocalDate startDate,
            LocalDate endDate
    ) {
    }
}
