package com.threelightstechnology.hotelbudapest.chambre_reservation.infrastructure.entrypoints;

import com.threelightstechnology.hotelbudapest.chambre_reservation.domain.chambre.Chambre;
import com.threelightstechnology.hotelbudapest.chambre_reservation.use_cases.GetChambresDisponibles;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;
import java.util.stream.Collectors;

@RestController
public class GetAvailableRoomsByEndpoint {
    private final GetChambresDisponibles getChambresDisponibles;

    public GetAvailableRoomsByEndpoint(GetChambresDisponibles getChambresDisponibles) {
        this.getChambresDisponibles = getChambresDisponibles;
    }

    @GetMapping(value = "/v1/search/rooms/{startDate}/{endDate}/{capacity}")
    List<ChambreResponseViewModel> execute(
            @PathVariable("startDate") LocalDate startDate,
            @PathVariable("endDate") LocalDate endDate,
            @PathVariable("capacity") int capacity
    ) {
        return getChambresDisponibles.handle(
                        capacity,
                        startDate.atStartOfDay(),
                        endDate.atStartOfDay()
                ).stream()
                .map(ChambreResponseViewModel::new)
                .collect(Collectors.toList());
    }

    private static final class ChambreResponseViewModel {
        private final Chambre chambre;

        public ChambreResponseViewModel(Chambre chambre) {
            this.chambre = chambre;
        }

        public String getNumber() {
            return chambre.numero();
        }

        public String getName() {
            return chambre.nom();
        }

        public int getCapacity() {
            return chambre.capacite();
        }

        public BigDecimal getPriceByNight() {
            return chambre.prixParNuit();
        }
    }
}
