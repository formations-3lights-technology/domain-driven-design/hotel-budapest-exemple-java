package com.threelightstechnology.hotelbudapest.chambre_reservation.infrastructure.gateways.reservation_repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component("chambre_reservation-reservation-data-source")
public interface ReservationDataSource extends JpaRepository<ReservationEntity, String> {
    Optional<ReservationEntity> findByIdIs(String id);
}
