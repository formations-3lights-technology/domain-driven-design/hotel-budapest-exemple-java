package com.threelightstechnology.hotelbudapest.chambre_reservation.infrastructure.entrypoints;

import com.threelightstechnology.hotelbudapest.chambre_reservation.domain.reservation.ReservationSnapshot;
import com.threelightstechnology.hotelbudapest.chambre_reservation.use_cases.GetReservationParId;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.math.BigDecimal;
import java.time.LocalDateTime;

@RestController
public class GetBookingByIdEndpoint {
    private final GetReservationParId getReservationParId;

    public GetBookingByIdEndpoint(GetReservationParId getReservationParId) {
        this.getReservationParId = getReservationParId;
    }

    @GetMapping(value = "/v1/booking/rooms/{id}")
    ReservationViewModel execute(
            @PathVariable("id") String id
    ) {
        return new ReservationViewModel(getReservationParId.handle(id));
    }

    private static final class ReservationViewModel {
        private final ReservationSnapshot snapshot;

        public ReservationViewModel(ReservationSnapshot snapshot) {
            this.snapshot = snapshot;
        }

        public String getId() {
            return snapshot.id();
        }

        public String getBookingEmail() {
            return snapshot.mailDeReservation();
        }

        public String getRoomNumber() {
            return snapshot.numeroDeLaChambre();
        }

        public int getTotalPerson() {
            return snapshot.nombreDePersonnes();
        }

        public BigDecimal getPriceByNight() {
            return snapshot.prixParNuit();
        }

        public LocalDateTime getStartDate() {
            return snapshot.dateDeDebut();
        }

        public LocalDateTime getEndDate() {
            return snapshot.dateDeFin();
        }
    }
}
