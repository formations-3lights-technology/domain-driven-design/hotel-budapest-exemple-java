package com.threelightstechnology.hotelbudapest.chambre_reservation.infrastructure.gateways.reservation_repository;

import com.threelightstechnology.hotelbudapest.chambre_reservation.domain.reservation.ReservationSnapshot;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Table;

import java.math.BigDecimal;
import java.time.LocalDateTime;

@Entity(name = "chambre_reservation-reservation")
@Table(schema = "chambre_reservation", name = "reservation")
public class ReservationEntity {
    @Id
    private String id;
    @Column(name = "numero_de_la_chambre")
    private String numeroDeLaChambre;
    private String mailDeReservation;
    private int nombreDePersonnes;
    private BigDecimal prixParNuit;
    private LocalDateTime dateDeDebut;
    private LocalDateTime dateDeFin;

    public ReservationEntity(
            String id,
            String numeroDeLaChambre,
            String mailDeReservation,
            int nombreDePersonnes,
            BigDecimal prixParNuit,
            LocalDateTime dateDeDebut,
            LocalDateTime dateDeFin
    ) {
        this.id = id;
        this.numeroDeLaChambre = numeroDeLaChambre;
        this.mailDeReservation = mailDeReservation;
        this.nombreDePersonnes = nombreDePersonnes;
        this.prixParNuit = prixParNuit;
        this.dateDeDebut = dateDeDebut;
        this.dateDeFin = dateDeFin;
    }

    public ReservationEntity() {
    }

    public static ReservationEntity mapDepuisSnapshot(ReservationSnapshot snapshot) {
        return new ReservationEntity(
                snapshot.id(),
                snapshot.numeroDeLaChambre(),
                snapshot.mailDeReservation(),
                snapshot.nombreDePersonnes(),
                snapshot.prixParNuit(),
                snapshot.dateDeDebut(),
                snapshot.dateDeFin()
        );
    }

    public ReservationSnapshot mapVersSnapshot() {
        return new ReservationSnapshot(
                id,
                mailDeReservation,
                numeroDeLaChambre,
                nombreDePersonnes,
                prixParNuit,
                dateDeDebut,
                dateDeFin
        );
    }
}
