package com.threelightstechnology.hotelbudapest.chambre_reservation.infrastructure.gateways.chambre_loader;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;

@Component("chambre_reservation-chambre-data-source")
public interface ChambreDataSource extends JpaRepository<ChambreEntity, String> {
    Optional<ChambreEntity> findByNumeroIs(String numero);

    List<ChambreEntity> findAllByCapaciteIsGreaterThanEqualOrderByNumeroAsc(int capacite);
}
