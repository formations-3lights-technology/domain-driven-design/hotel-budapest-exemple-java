package com.threelightstechnology.hotelbudapest.chambre_reservation.infrastructure.gateways.chambre_loader;

import com.threelightstechnology.hotelbudapest.chambre_reservation.domain.chambre.Chambre;
import com.threelightstechnology.hotelbudapest.chambre_reservation.domain.chambre.ChambreReservation;
import com.threelightstechnology.hotelbudapest.chambre_reservation.infrastructure.gateways.reservation_repository.ReservationEntity;
import jakarta.persistence.*;

import java.math.BigDecimal;
import java.util.List;
import java.util.stream.Collectors;

@Entity(name = "chambre_reservation-chambre")
@Table(schema = "chambre_reservation", name = "chambre")
public class ChambreEntity {
    @Id
    private String numero;
    private String nom;
    private int capacite;
    private BigDecimal prixParNuit;

    @OneToMany(targetEntity = ReservationEntity.class, fetch = FetchType.EAGER)
    @JoinColumn(name = "numero_de_la_chambre", referencedColumnName = "numero")
    private List<ReservationEntity> reservations;

    public ChambreEntity(
            String numero,
            String nom,
            int capacite,
            BigDecimal prixParNuit,
            List<ReservationEntity> reservations
    ) {
        this.numero = numero;
        this.nom = nom;
        this.capacite = capacite;
        this.prixParNuit = prixParNuit;
        this.reservations = reservations;
    }

    public ChambreEntity() {
    }

    public Chambre toChambre() {
        return new Chambre(
                numero,
                nom,
                capacite,
                prixParNuit,
                reservations.stream()
                        .map(r -> new ChambreReservation(
                                r.mapVersSnapshot().dateDeDebut(),
                                r.mapVersSnapshot().dateDeFin()
                        ))
                        .collect(Collectors.toList())
        );
    }
}
