package com.threelightstechnology.hotelbudapest.chambre_reservation.infrastructure.gateways.chambre_loader;

import com.threelightstechnology.hotelbudapest.chambre_reservation.domain.chambre.Chambre;
import com.threelightstechnology.hotelbudapest.chambre_reservation.domain.chambre.ChambreLoader;
import com.threelightstechnology.hotelbudapest.chambre_reservation.domain.chambre.ChambreNonTrouveeException;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

@Repository("chambre_reservation-chambre_loader")
public class JpaChambreLoader implements ChambreLoader {

    private final ChambreDataSource chambreDataSource;

    public JpaChambreLoader(ChambreDataSource chambreDataSource) {
        this.chambreDataSource = chambreDataSource;
    }

    public Chambre getParNumeroOuErreur(String numero) {
        return chambreDataSource.findByNumeroIs(numero)
                .map(ChambreEntity::toChambre)
                .orElseThrow(ChambreNonTrouveeException::new);
    }

    public List<Chambre> getPar(int capacite, LocalDateTime dateDeDebut, LocalDateTime dateDeFin) {
        return chambreDataSource.findAllByCapaciteIsGreaterThanEqualOrderByNumeroAsc(capacite)
                .stream()
                .map(ChambreEntity::toChambre)
                .filter(c -> c.reservations().stream()
                        .noneMatch(r -> isDateRangeOverlap(r.dateDeDebut(), r.dateDeFin(), dateDeDebut, dateDeFin))
                )
                .collect(Collectors.toList());
    }

    private boolean isDateRangeOverlap(LocalDateTime start1, LocalDateTime end1, LocalDateTime start2, LocalDateTime end2) {
        return !end1.isBefore(start2) && !end2.isBefore(start1);
    }
}
