package com.threelightstechnology.hotelbudapest;

import com.threelightstechnology.hotelbudapest.chambre_reservation.domain.chambre.ChambreLoader;
import com.threelightstechnology.hotelbudapest.chambre_reservation.domain.reservation.ReservationRepository;
import com.threelightstechnology.hotelbudapest.chambre_reservation.use_cases.reserver_chambre.ReserverUneChambreCommand;
import com.threelightstechnology.hotelbudapest.chambre_reservation.use_cases.reserver_chambre.ReserverUneChambreCommandHandler;
import com.threelightstechnology.hotelbudapest.deroulement_sejour.domain.reservation.ReservationLoader;
import com.threelightstechnology.hotelbudapest.deroulement_sejour.domain.sejour.SejourRepository;
import com.threelightstechnology.hotelbudapest.deroulement_sejour.domain.sejour.events.EnvoiFactureDemandeEvent;
import com.threelightstechnology.hotelbudapest.deroulement_sejour.domain.sejour.events.SejourTermineEvent;
import com.threelightstechnology.hotelbudapest.deroulement_sejour.use_cases.demarrer_sejour.DemarrerUnSejourCommand;
import com.threelightstechnology.hotelbudapest.deroulement_sejour.use_cases.demarrer_sejour.DemarrerUnSejourCommandHandler;
import com.threelightstechnology.hotelbudapest.deroulement_sejour.use_cases.envoyer_questionnaire_de_satisfaction.EnvoyerQuestionnaireDeSatisfactionCommand;
import com.threelightstechnology.hotelbudapest.deroulement_sejour.use_cases.envoyer_questionnaire_de_satisfaction.EnvoyerQuestionnaireDeSatisfactionCommandHandler;
import com.threelightstechnology.hotelbudapest.deroulement_sejour.use_cases.finir_sejour.commad_handler.FinirUnSejourCommand;
import com.threelightstechnology.hotelbudapest.deroulement_sejour.use_cases.finir_sejour.commad_handler.FinirUnSejourCommandHandler;
import com.threelightstechnology.hotelbudapest.deroulement_sejour.use_cases.finir_sejour.event_listeners.EnvoyerUneFactureEventListener;
import com.threelightstechnology.hotelbudapest.deroulement_sejour.use_cases.finir_sejour.event_listeners.NotifierServiceDEntretienEventListener;
import com.threelightstechnology.hotelbudapest.facturation.domain.MailSender;
import com.threelightstechnology.hotelbudapest.facturation.use_cases.EnvoyerUneFactureCommand;
import com.threelightstechnology.hotelbudapest.facturation.use_cases.EnvoyerUneFactureCommandHandler;
import com.threelightstechnology.hotelbudapest.shared.command_disptacher.CommandDispatcher;
import com.threelightstechnology.hotelbudapest.shared.command_disptacher.InternalCommandDispatcher;
import com.threelightstechnology.hotelbudapest.shared.event_publisher.EventPublisher;
import com.threelightstechnology.hotelbudapest.shared.event_publisher.InternalEventPublisher;
import com.threelightstechnology.hotelbudapest.shared.infrastructure.gateways.http_client.InvoicingServiceHttpClient;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.Map;
import java.util.Set;

@Configuration
public class ApplicationConfiguration {
    private final ReservationRepository reservationRepository;
    private final ChambreLoader chambreLoader;

    private final SejourRepository sejourRepository;
    private final ReservationLoader reservationLoader;

    private final InvoicingServiceHttpClient invoicingServiceHttpClient;
    private final MailSender mailSender;

    public ApplicationConfiguration(
            ReservationRepository reservationRepository,
            ChambreLoader chambreLoader,
            SejourRepository sejourRepository,
            ReservationLoader reservationLoader,
            InvoicingServiceHttpClient invoicingServiceHttpClient,
            MailSender mailSender
    ) {
        this.reservationRepository = reservationRepository;
        this.chambreLoader = chambreLoader;
        this.sejourRepository = sejourRepository;
        this.reservationLoader = reservationLoader;
        this.invoicingServiceHttpClient = invoicingServiceHttpClient;
        this.mailSender = mailSender;
    }

    @Bean
    public CommandDispatcher setupCommandDispatcher() {
        EventPublisher eventPublisher = new InternalEventPublisher()
                .registerListeners(Map.of(
                        EnvoiFactureDemandeEvent.label, Set.of(new EnvoyerUneFactureEventListener(reservationLoader, invoicingServiceHttpClient)),
                        SejourTermineEvent.label, Set.of(new NotifierServiceDEntretienEventListener())
                ));

        return new InternalCommandDispatcher()
                .registerHandlers(Map.of(
                        ReserverUneChambreCommand.label, new ReserverUneChambreCommandHandler(reservationRepository, chambreLoader, eventPublisher),

                        DemarrerUnSejourCommand.label, new DemarrerUnSejourCommandHandler(sejourRepository, reservationLoader, eventPublisher),
                        FinirUnSejourCommand.label, new FinirUnSejourCommandHandler(sejourRepository, eventPublisher),
                        EnvoyerQuestionnaireDeSatisfactionCommand.label, new EnvoyerQuestionnaireDeSatisfactionCommandHandler(sejourRepository, reservationLoader, mailSender),

                        EnvoyerUneFactureCommand.label, new EnvoyerUneFactureCommandHandler(mailSender)
                ));
    }
}
