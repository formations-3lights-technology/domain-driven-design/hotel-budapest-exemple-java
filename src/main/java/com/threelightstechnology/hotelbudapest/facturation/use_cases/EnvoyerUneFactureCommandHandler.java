package com.threelightstechnology.hotelbudapest.facturation.use_cases;

import com.threelightstechnology.hotelbudapest.facturation.domain.Mail;
import com.threelightstechnology.hotelbudapest.facturation.domain.MailSender;
import com.threelightstechnology.hotelbudapest.shared.command_disptacher.CommandHandler;

public class EnvoyerUneFactureCommandHandler implements CommandHandler<EnvoyerUneFactureCommand> {
    private final MailSender mailSender;

    public EnvoyerUneFactureCommandHandler(MailSender mailSender) {
        this.mailSender = mailSender;
    }

    public void handle(EnvoyerUneFactureCommand command) {
        mailSender.send(new Mail(
                "service-facturation@email.com",
                command.getMailDeFacturation(),
                "Facture d'un montant de : %s EUR".formatted(command.getMontant())
        ));
    }
}
