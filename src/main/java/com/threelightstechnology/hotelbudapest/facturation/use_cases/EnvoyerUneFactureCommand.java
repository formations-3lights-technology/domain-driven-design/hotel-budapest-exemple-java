package com.threelightstechnology.hotelbudapest.facturation.use_cases;

import com.threelightstechnology.hotelbudapest.shared.command_disptacher.Command;

import java.math.BigDecimal;
import java.util.Objects;

public class EnvoyerUneFactureCommand extends Command {
    public static final String label = "SendInvoiceCommand";

    private final String mailDeFacturation;
    private final BigDecimal montant;

    public EnvoyerUneFactureCommand(String mailDeFacturation, BigDecimal montant) {
        this.mailDeFacturation = mailDeFacturation;
        this.montant = montant;
    }

    public String label() {
        return EnvoyerUneFactureCommand.label;
    }

    public String getMailDeFacturation() {
        return mailDeFacturation;
    }

    public BigDecimal getMontant() {
        return montant;
    }

    public boolean equals(Object o) {
        if (this==o) return true;
        if (o==null || getClass()!=o.getClass()) return false;
        EnvoyerUneFactureCommand that = (EnvoyerUneFactureCommand) o;
        return Objects.equals(mailDeFacturation, that.mailDeFacturation) && Objects.equals(montant, that.montant);
    }
}
