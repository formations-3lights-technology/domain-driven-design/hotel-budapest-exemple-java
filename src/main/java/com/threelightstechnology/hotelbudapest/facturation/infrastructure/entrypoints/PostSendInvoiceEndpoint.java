package com.threelightstechnology.hotelbudapest.facturation.infrastructure.entrypoints;

import com.threelightstechnology.hotelbudapest.facturation.use_cases.EnvoyerUneFactureCommand;
import com.threelightstechnology.hotelbudapest.shared.command_disptacher.CommandDispatcher;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import java.math.BigDecimal;

@RestController
public class PostSendInvoiceEndpoint {
    private final CommandDispatcher commandDispatcher;

    public PostSendInvoiceEndpoint(CommandDispatcher commandDispatcher) {
        this.commandDispatcher = commandDispatcher;
    }

    @PostMapping(value = "/v1/invoices/send")
    @ResponseStatus(HttpStatus.ACCEPTED)
    void execute(@RequestBody Body body) {
        commandDispatcher.dispatch(
                new EnvoyerUneFactureCommand(
                        body.invoiceEmail(),
                        body.amount()
                )
        );
    }

    private record Body(String invoiceEmail, BigDecimal amount) {
    }
}
