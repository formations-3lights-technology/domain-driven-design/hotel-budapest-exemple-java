package com.threelightstechnology.hotelbudapest.facturation.infrastructure.gateways;

import com.threelightstechnology.hotelbudapest.facturation.domain.Mail;
import com.threelightstechnology.hotelbudapest.facturation.domain.MailSender;
import org.springframework.stereotype.Component;

@Component
public class FakeMailSender implements MailSender {
    public void send(Mail mail) {
        System.out.println(mail);
    }
}
