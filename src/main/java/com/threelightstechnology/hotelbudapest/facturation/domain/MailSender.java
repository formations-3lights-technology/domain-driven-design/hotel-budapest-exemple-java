package com.threelightstechnology.hotelbudapest.facturation.domain;

public interface MailSender {
    void send(Mail mail);
}
