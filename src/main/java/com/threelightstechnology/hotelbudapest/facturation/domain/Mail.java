package com.threelightstechnology.hotelbudapest.facturation.domain;

public record Mail(String from, String to, String message) {
}
