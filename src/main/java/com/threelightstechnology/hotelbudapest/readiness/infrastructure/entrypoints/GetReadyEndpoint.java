package com.threelightstechnology.hotelbudapest.readiness.infrastructure.entrypoints;

import com.threelightstechnology.hotelbudapest.readiness.domain.ReadyGlobalStatus;
import com.threelightstechnology.hotelbudapest.readiness.usecases.ReadyChecker;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class GetReadyEndpoint {
    private final ReadyChecker readyChecker;

    public GetReadyEndpoint(ReadyChecker readyChecker) {
        this.readyChecker = readyChecker;
    }

    @GetMapping(value = "/v1/ready")
    ReadyCheckResponseViewModel execute() {
        return new ReadyCheckResponseViewModel(readyChecker.handle());
    }

    private static final class ReadyCheckResponseViewModel {
        private final ReadyGlobalStatus apiGlobalStatus;

        private ReadyCheckResponseViewModel(ReadyGlobalStatus apiGlobalStatus) {
            this.apiGlobalStatus = apiGlobalStatus;
        }

        public String getApiVersion() {
            return apiGlobalStatus.getStatut().getApiVersion();
        }

        public String getApiStatus() {
            return apiGlobalStatus.getStatut().getApiStatus().name();
        }
    }
}
