package com.threelightstechnology.hotelbudapest.readiness.usecases;

import com.threelightstechnology.hotelbudapest.readiness.domain.ReadyGlobalStatus;
import org.springframework.stereotype.Service;

@Service
public class ReadyChecker {
    public ReadyGlobalStatus handle() {
        return new ReadyGlobalStatus();
    }
}
