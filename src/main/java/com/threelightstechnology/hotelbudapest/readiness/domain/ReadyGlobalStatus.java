package com.threelightstechnology.hotelbudapest.readiness.domain;

public class ReadyGlobalStatus {
    public ReadyGlobalAllStatus getStatut() {
        return new ReadyGlobalAllStatus("1.0.0", ApiStatus.UP);
    }

    public boolean isUp() {
        return true;
    }
}
