package com.threelightstechnology.hotelbudapest.readiness.domain;

public enum ApiStatus {
    UP,
    DOWN
}
