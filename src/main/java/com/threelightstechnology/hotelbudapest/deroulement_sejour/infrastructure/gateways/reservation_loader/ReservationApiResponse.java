package com.threelightstechnology.hotelbudapest.deroulement_sejour.infrastructure.gateways.reservation_loader;

import java.math.BigDecimal;
import java.time.LocalDateTime;

public record ReservationApiResponse(
        String id,
        String bookingEmail,
        String roomNumber,
        int totalPerson,
        BigDecimal priceByNight,
        LocalDateTime startDate,
        LocalDateTime endDate
) {
}
