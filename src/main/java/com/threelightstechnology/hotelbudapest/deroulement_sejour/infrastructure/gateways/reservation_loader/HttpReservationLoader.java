package com.threelightstechnology.hotelbudapest.deroulement_sejour.infrastructure.gateways.reservation_loader;

import com.threelightstechnology.hotelbudapest.deroulement_sejour.domain.reservation.Reservation;
import com.threelightstechnology.hotelbudapest.deroulement_sejour.domain.reservation.ReservationLoader;
import com.threelightstechnology.hotelbudapest.deroulement_sejour.domain.reservation.ReservationNonTrouveeException;
import com.threelightstechnology.hotelbudapest.shared.domain.http_client.HttpClient;
import com.threelightstechnology.hotelbudapest.shared.domain.http_client.HttpResponse;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

@Component
public class HttpReservationLoader implements ReservationLoader {
    private final HttpClient httpClient;

    public HttpReservationLoader(@Qualifier("roomBookingServiceHttpClient") HttpClient httpClient) {
        this.httpClient = httpClient;
    }

    public Reservation getParIdOuErreur(String id) {
        try {
            HttpResponse<ReservationApiResponse> response = httpClient.get("/booking/rooms/%s".formatted(id));
            return new Reservation(
                    response.body().id(),
                    response.body().bookingEmail(),
                    response.body().priceByNight(),
                    response.body().startDate(),
                    response.body().endDate()
            );
        } catch (Exception e) {
            throw new ReservationNonTrouveeException();
        }
    }
}
