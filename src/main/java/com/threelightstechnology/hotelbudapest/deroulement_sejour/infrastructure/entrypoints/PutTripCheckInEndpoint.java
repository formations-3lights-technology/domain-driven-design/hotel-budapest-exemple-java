package com.threelightstechnology.hotelbudapest.deroulement_sejour.infrastructure.entrypoints;

import com.threelightstechnology.hotelbudapest.deroulement_sejour.use_cases.demarrer_sejour.DemarrerUnSejourCommand;
import com.threelightstechnology.hotelbudapest.shared.command_disptacher.CommandDispatcher;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDate;

@RestController
public class PutTripCheckInEndpoint {
    private final CommandDispatcher commandDispatcher;

    public PutTripCheckInEndpoint(CommandDispatcher commandDispatcher) {
        this.commandDispatcher = commandDispatcher;
    }

    @PutMapping(value = "/v1/trips/check-in")
    @ResponseStatus(HttpStatus.ACCEPTED)
    void execute(@RequestBody Body body) {
        commandDispatcher.dispatch(new DemarrerUnSejourCommand(
                body.bookingId(),
                body.startDate().atStartOfDay()
        ));
    }

    private record Body(
            String bookingId,
            LocalDate startDate
    ) {
    }
}
