package com.threelightstechnology.hotelbudapest.deroulement_sejour.infrastructure.entrypoints;

import com.threelightstechnology.hotelbudapest.deroulement_sejour.use_cases.finir_sejour.commad_handler.FinirUnSejourCommand;
import com.threelightstechnology.hotelbudapest.shared.command_disptacher.CommandDispatcher;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Optional;

@RestController
public class PutTripCheckOutEndpoint {
    private final CommandDispatcher commandDispatcher;

    public PutTripCheckOutEndpoint(CommandDispatcher commandDispatcher) {
        this.commandDispatcher = commandDispatcher;
    }

    @PutMapping(value = "/v1/trips/{id}/check-out")
    @ResponseStatus(HttpStatus.ACCEPTED)
    void execute(
            @PathVariable(name = "id") String id,
            @RequestBody Body body
    ) {
        commandDispatcher.dispatch(new FinirUnSejourCommand(
                id,
                body.endDate.atStartOfDay(),
                body.amount,
                body.invoiceEmail
        ));
    }

    private record Body(
            BigDecimal amount,
            LocalDate endDate,
            Optional<String> invoiceEmail
    ) {
    }
}
