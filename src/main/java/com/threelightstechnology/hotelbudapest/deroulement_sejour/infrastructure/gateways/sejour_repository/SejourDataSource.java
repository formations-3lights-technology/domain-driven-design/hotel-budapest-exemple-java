package com.threelightstechnology.hotelbudapest.deroulement_sejour.infrastructure.gateways.sejour_repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

@Component("deroulement_sejour-sejout-data-source")
public interface SejourDataSource extends JpaRepository<SejourEntity, String> {
    Optional<SejourEntity> findByIdIs(String id);

    List<SejourEntity> findAllByDateDeFinIsBetween(LocalDateTime dateDeFinDebut, LocalDateTime dateDeFinFin);
}
