package com.threelightstechnology.hotelbudapest.deroulement_sejour.infrastructure.gateways.sejour_repository;

import com.threelightstechnology.hotelbudapest.deroulement_sejour.domain.sejour.SejourRepository;
import com.threelightstechnology.hotelbudapest.deroulement_sejour.domain.sejour.SejourSnapshot;
import com.threelightstechnology.hotelbudapest.deroulement_sejour.domain.sejour.exceptions.SejourNonTrouveException;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.List;
import java.util.UUID;

@Repository("deroulement_sejour-sejour_repository")
public class JpaSejourRepository implements SejourRepository {
    private final SejourDataSource sejourDataSource;

    public JpaSejourRepository(SejourDataSource sejourDataSource) {
        this.sejourDataSource = sejourDataSource;
    }

    public String nextId() {
        return UUID.randomUUID().toString();
    }

    public void save(SejourSnapshot sejour) {
        sejourDataSource.save(SejourEntity.mapDepuisSnapshot(sejour));
    }

    public SejourSnapshot getParIdOuErreur(String id) {
        return sejourDataSource.findByIdIs(id)
                .map(SejourEntity::mapVersSnapshot)
                .orElseThrow(SejourNonTrouveException::new);
    }

    public List<SejourSnapshot> getParDateDeFin(LocalDate date) {
        return sejourDataSource.findAllByDateDeFinIsBetween(date.atStartOfDay(), date.plusDays(1).atStartOfDay())
                .stream()
                .map(SejourEntity::mapVersSnapshot)
                .toList();
    }
}
