package com.threelightstechnology.hotelbudapest.deroulement_sejour.infrastructure.entrypoints;

import com.threelightstechnology.hotelbudapest.deroulement_sejour.domain.sejour.SejourSnapshot;
import com.threelightstechnology.hotelbudapest.deroulement_sejour.use_cases.GetSejourParId;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.math.BigDecimal;
import java.time.LocalDateTime;

@RestController
public class GetTripByIdEndpoint {
    private final GetSejourParId getSejourParId;

    public GetTripByIdEndpoint(GetSejourParId getSejourParId) {
        this.getSejourParId = getSejourParId;
    }

    @GetMapping(value = "/v1/trips/{id}")
    SejourResponseViewModel execute(
            @PathVariable("id") String id
    ) {
        return new SejourResponseViewModel(getSejourParId.handle(id));
    }

    private static final class SejourResponseViewModel {
        private final SejourSnapshot snapshot;

        public SejourResponseViewModel(SejourSnapshot snapshot) {
            this.snapshot = snapshot;
        }

        public String getId() {
            return snapshot.id();
        }

        public String getBookingId() {
            return snapshot.reservationId();
        }

        public BigDecimal getPriceByNight() {
            return snapshot.prixParNuit();
        }

        public LocalDateTime getCheckInDate() {
            return snapshot.dateDEnregistrement();
        }

        public LocalDateTime getExpectedEndDate() {
            return snapshot.dateDeFinPrevue();
        }

        public LocalDateTime getEndDate() {
            return snapshot.dateDeFin().orElse(null);
        }

        public BigDecimal getAmount() {
            return snapshot.montantPaye().orElse(null);
        }
    }
}
