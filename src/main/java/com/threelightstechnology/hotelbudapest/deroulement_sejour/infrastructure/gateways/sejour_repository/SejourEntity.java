package com.threelightstechnology.hotelbudapest.deroulement_sejour.infrastructure.gateways.sejour_repository;

import com.threelightstechnology.hotelbudapest.deroulement_sejour.domain.sejour.SejourSnapshot;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Table;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Optional;

@Entity(name = "deroulement_sejour-sejour")
@Table(schema = "deroulement_sejour", name = "sejour")
public class SejourEntity {
    @Id
    private String id;
    private String reservationId;
    private BigDecimal prixParNuit;
    @Column(name = "date_d_enregistrement")
    private LocalDateTime dateDEnregistrement;
    private LocalDateTime dateDeFinPrevue;
    private LocalDateTime dateDeFin;
    private BigDecimal montantPaye;

    public SejourEntity(
            String id,
            String reservationId,
            BigDecimal prixParNuit,
            LocalDateTime dateDEnregistrement,
            LocalDateTime dateDeFinPrevue,
            LocalDateTime dateDeFin,
            BigDecimal montantPaye
    ) {
        this.id = id;
        this.reservationId = reservationId;
        this.prixParNuit = prixParNuit;
        this.dateDEnregistrement = dateDEnregistrement;
        this.dateDeFinPrevue = dateDeFinPrevue;
        this.dateDeFin = dateDeFin;
        this.montantPaye = montantPaye;
    }

    public SejourEntity() {
    }

    public static SejourEntity mapDepuisSnapshot(SejourSnapshot snapshot) {
        return new SejourEntity(
                snapshot.id(),
                snapshot.reservationId(),
                snapshot.prixParNuit(),
                snapshot.dateDEnregistrement(),
                snapshot.dateDeFinPrevue(),
                snapshot.dateDeFin().orElse(null),
                snapshot.montantPaye().orElse(null)
        );
    }

    public SejourSnapshot mapVersSnapshot() {
        return new SejourSnapshot(
                id,
                reservationId,
                prixParNuit,
                dateDEnregistrement,
                dateDeFinPrevue,
                Optional.ofNullable(dateDeFin),
                Optional.ofNullable(montantPaye)
        );
    }
}
