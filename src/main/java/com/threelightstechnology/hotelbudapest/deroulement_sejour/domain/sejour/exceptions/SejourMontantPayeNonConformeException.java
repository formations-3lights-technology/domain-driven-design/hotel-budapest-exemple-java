package com.threelightstechnology.hotelbudapest.deroulement_sejour.domain.sejour.exceptions;

import com.threelightstechnology.hotelbudapest.shared.domain.erreurs.ErreurDeValidation;

public class SejourMontantPayeNonConformeException extends ErreurDeValidation {
    public SejourMontantPayeNonConformeException() {
        super("Amount is not good");
    }
}
