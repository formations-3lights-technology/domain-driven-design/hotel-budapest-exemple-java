package com.threelightstechnology.hotelbudapest.deroulement_sejour.domain.reservation;

public interface ReservationLoader {
    Reservation getParIdOuErreur(String id);
}
