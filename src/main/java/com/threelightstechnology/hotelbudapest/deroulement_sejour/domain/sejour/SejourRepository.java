package com.threelightstechnology.hotelbudapest.deroulement_sejour.domain.sejour;

import java.time.LocalDate;
import java.util.List;

public interface SejourRepository {
    String nextId();

    void save(SejourSnapshot sejour);

    SejourSnapshot getParIdOuErreur(String id);

    List<SejourSnapshot> getParDateDeFin(LocalDate date);
}
