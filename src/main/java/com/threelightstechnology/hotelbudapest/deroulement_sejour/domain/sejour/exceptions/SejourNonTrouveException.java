package com.threelightstechnology.hotelbudapest.deroulement_sejour.domain.sejour.exceptions;

import com.threelightstechnology.hotelbudapest.shared.domain.erreurs.ElementNonTrouve;

public class SejourNonTrouveException extends ElementNonTrouve {
    public SejourNonTrouveException() {
        super("Trip is not found");
    }
}
