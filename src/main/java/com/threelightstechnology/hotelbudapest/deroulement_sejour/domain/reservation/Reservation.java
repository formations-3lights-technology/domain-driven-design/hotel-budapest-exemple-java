package com.threelightstechnology.hotelbudapest.deroulement_sejour.domain.reservation;

import java.math.BigDecimal;
import java.time.LocalDateTime;

public record Reservation(
        String id,
        String mailDeReservation,
        BigDecimal prixParNuit,
        LocalDateTime dateDeDebut,
        LocalDateTime dateDeFin
) {
}
