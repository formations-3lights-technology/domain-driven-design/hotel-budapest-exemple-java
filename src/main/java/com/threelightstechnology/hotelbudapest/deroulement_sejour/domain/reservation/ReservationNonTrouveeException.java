package com.threelightstechnology.hotelbudapest.deroulement_sejour.domain.reservation;

import com.threelightstechnology.hotelbudapest.shared.domain.erreurs.ElementNonTrouve;

public class ReservationNonTrouveeException extends ElementNonTrouve {
    public ReservationNonTrouveeException() {
        super("Booking is not found");
    }
}
