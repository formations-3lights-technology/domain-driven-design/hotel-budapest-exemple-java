package com.threelightstechnology.hotelbudapest.deroulement_sejour.domain.sejour.events;

import com.threelightstechnology.hotelbudapest.shared.event_publisher.DomainEvent;

import java.time.LocalDateTime;
import java.util.Objects;

public class SejourTermineEvent extends DomainEvent {
    public static final String label = "TripCheckOutEvent";
    private final String id;
    private final LocalDateTime dateDeFin;

    public SejourTermineEvent(
            String id,
            LocalDateTime dateDeFin
    ) {
        this.id = id;
        this.dateDeFin = dateDeFin;
    }

    public String label() {
        return SejourTermineEvent.label;
    }

    public boolean equals(Object o) {
        if (this==o) return true;
        if (o==null || getClass()!=o.getClass()) return false;
        SejourTermineEvent that = (SejourTermineEvent) o;
        return Objects.equals(id, that.id) && Objects.equals(dateDeFin, that.dateDeFin);
    }
}
