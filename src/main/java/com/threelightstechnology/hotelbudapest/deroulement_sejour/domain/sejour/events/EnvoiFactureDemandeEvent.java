package com.threelightstechnology.hotelbudapest.deroulement_sejour.domain.sejour.events;

import com.threelightstechnology.hotelbudapest.shared.event_publisher.DomainEvent;

import java.math.BigDecimal;
import java.util.Objects;
import java.util.Optional;

public class EnvoiFactureDemandeEvent extends DomainEvent {
    public static final String label = "SendInvoiceAskedEvent";
    private final String sejourId;
    private final String reservationId;
    private final Optional<String> mailDeFacturation;
    private final BigDecimal amount;

    public EnvoiFactureDemandeEvent(
            String sejourId,
            String reservationId,
            Optional<String> mailDeFacturation,
            BigDecimal amount
    ) {
        this.sejourId = sejourId;
        this.reservationId = reservationId;
        this.mailDeFacturation = mailDeFacturation;
        this.amount = amount;
    }

    public String label() {
        return EnvoiFactureDemandeEvent.label;
    }

    public String getSejourId() {
        return sejourId;
    }

    public String getReservationId() {
        return reservationId;
    }

    public Optional<String> getMailDeFacturation() {
        return mailDeFacturation;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public boolean equals(Object o) {
        if (this==o) return true;
        if (o==null || getClass()!=o.getClass()) return false;
        EnvoiFactureDemandeEvent that = (EnvoiFactureDemandeEvent) o;
        return Objects.equals(sejourId, that.sejourId) && Objects.equals(reservationId, that.reservationId) && Objects.equals(mailDeFacturation, that.mailDeFacturation) && Objects.equals(amount, that.amount);
    }
}
