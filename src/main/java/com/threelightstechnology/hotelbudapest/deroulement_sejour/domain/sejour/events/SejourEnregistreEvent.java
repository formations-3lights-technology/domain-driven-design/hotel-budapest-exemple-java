package com.threelightstechnology.hotelbudapest.deroulement_sejour.domain.sejour.events;

import com.threelightstechnology.hotelbudapest.shared.event_publisher.DomainEvent;

import java.time.LocalDateTime;
import java.util.Objects;

public class SejourEnregistreEvent extends DomainEvent {
    static final String label = "TripCheckInEvent";
    private final String id;
    private final String reservationId;
    private final LocalDateTime dateDEnregistrement;

    public SejourEnregistreEvent(
            String id,
            String reservationId,
            LocalDateTime dateDEnregistrement
    ) {
        this.id = id;
        this.reservationId = reservationId;
        this.dateDEnregistrement = dateDEnregistrement;
    }

    public String label() {
        return SejourEnregistreEvent.label;
    }

    public boolean equals(Object o) {
        if (this==o) return true;
        if (o==null || getClass()!=o.getClass()) return false;
        SejourEnregistreEvent that = (SejourEnregistreEvent) o;
        return Objects.equals(id, that.id) && Objects.equals(reservationId, that.reservationId) && Objects.equals(dateDEnregistrement, that.dateDEnregistrement);
    }
}
