package com.threelightstechnology.hotelbudapest.deroulement_sejour.domain.sejour;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Optional;

public record SejourSnapshot(
        String id,
        String reservationId,
        BigDecimal prixParNuit,
        LocalDateTime dateDEnregistrement,
        LocalDateTime dateDeFinPrevue,
        Optional<LocalDateTime> dateDeFin,
        Optional<BigDecimal> montantPaye
) {
}
