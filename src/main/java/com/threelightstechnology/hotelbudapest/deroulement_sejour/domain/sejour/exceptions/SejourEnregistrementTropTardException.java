package com.threelightstechnology.hotelbudapest.deroulement_sejour.domain.sejour.exceptions;

import com.threelightstechnology.hotelbudapest.shared.domain.erreurs.ErreurDeValidation;

public class SejourEnregistrementTropTardException extends ErreurDeValidation {
    public SejourEnregistrementTropTardException() {
        super("Check in is too late");
    }
}
