package com.threelightstechnology.hotelbudapest.deroulement_sejour.domain.sejour;

import com.threelightstechnology.hotelbudapest.deroulement_sejour.domain.sejour.events.EnvoiFactureDemandeEvent;
import com.threelightstechnology.hotelbudapest.deroulement_sejour.domain.sejour.events.SejourEnregistreEvent;
import com.threelightstechnology.hotelbudapest.deroulement_sejour.domain.sejour.events.SejourTermineEvent;
import com.threelightstechnology.hotelbudapest.deroulement_sejour.domain.sejour.exceptions.SejourEnregistrementTropTardException;
import com.threelightstechnology.hotelbudapest.deroulement_sejour.domain.sejour.exceptions.SejourMontantPayeNonConformeException;
import com.threelightstechnology.hotelbudapest.shared.domain.Aggregate;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.Optional;

public class Sejour extends Aggregate {
    private final String id;
    private final String reservationId;
    private final BigDecimal prixParNuit;
    private final LocalDateTime dateDEnregistrement;
    private final LocalDateTime dateDeFinPrevue;
    private Optional<LocalDateTime> dateDeFin;
    private Optional<BigDecimal> montantPaye;

    private Sejour(
            String id,
            String reservationId,
            BigDecimal prixParNuit,
            LocalDateTime dateDEnregistrement,
            LocalDateTime dateDeFinPrevue,
            Optional<LocalDateTime> dateDeFin,
            Optional<BigDecimal> montantPaye
    ) {
        this.id = id;
        this.reservationId = reservationId;
        this.prixParNuit = prixParNuit;
        this.dateDEnregistrement = dateDEnregistrement;
        this.dateDeFinPrevue = dateDeFinPrevue;
        this.dateDeFin = dateDeFin;
        this.montantPaye = montantPaye;
    }

    public static Sejour enregistrement(
            String id,
            String reservationId,
            BigDecimal prixParNuit,
            LocalDateTime dateDEnregistrement,
            LocalDateTime dateDeDebutDeLaReservation,
            LocalDateTime dateDeFinDeLaReservation
    ) {
        long differenceEnJours = ChronoUnit.DAYS.between(dateDeDebutDeLaReservation, dateDEnregistrement);
        if (differenceEnJours > 1) {
            throw new SejourEnregistrementTropTardException();
        }

        Sejour sejour = new Sejour(
                id,
                reservationId,
                prixParNuit,
                dateDEnregistrement,
                dateDeFinDeLaReservation,
                Optional.empty(),
                Optional.empty()
        );

        sejour.apply(new SejourEnregistreEvent(id, reservationId, dateDEnregistrement));

        return sejour;
    }

    public static Sejour restore(SejourSnapshot snapshot) {
        return new Sejour(
                snapshot.id(),
                snapshot.reservationId(),
                snapshot.prixParNuit(),
                snapshot.dateDEnregistrement(),
                snapshot.dateDeFinPrevue(),
                snapshot.dateDeFin(),
                snapshot.montantPaye()
        );
    }

    public void terminer(LocalDateTime dateDeFin, String mailDeFacturation, BigDecimal montant) {
        BigDecimal dureeDuSejour = BigDecimal.valueOf(ChronoUnit.DAYS.between(dateDEnregistrement, dateDeFinPrevue));
        BigDecimal montantDuSejour = dureeDuSejour.abs().multiply(prixParNuit);
        if (!montantDuSejour.equals(montant)) {
            throw new SejourMontantPayeNonConformeException();
        }

        this.dateDeFin = Optional.of(dateDeFin);
        this.montantPaye = Optional.of(montant);

        apply(new SejourTermineEvent(id, dateDeFin));
        apply(new EnvoiFactureDemandeEvent(id, reservationId, Optional.ofNullable(mailDeFacturation), montant));
    }

    public SejourSnapshot getSnapshot() {
        return new SejourSnapshot(
                id,
                reservationId,
                prixParNuit,
                dateDEnregistrement,
                dateDeFinPrevue,
                dateDeFin,
                montantPaye
        );
    }
}
