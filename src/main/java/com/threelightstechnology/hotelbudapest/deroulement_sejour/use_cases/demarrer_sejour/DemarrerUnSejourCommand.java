package com.threelightstechnology.hotelbudapest.deroulement_sejour.use_cases.demarrer_sejour;

import com.threelightstechnology.hotelbudapest.shared.command_disptacher.Command;

import java.time.LocalDateTime;
import java.util.Objects;

public class DemarrerUnSejourCommand extends Command {
    public static final String label = "StartTripCommand";
    private final String reservationId;
    private final LocalDateTime dateDeDebut;

    public DemarrerUnSejourCommand(
            String reservationId,
            LocalDateTime dateDeDebut
    ) {
        super();
        this.reservationId = reservationId;
        this.dateDeDebut = dateDeDebut;
    }

    public String label() {
        return DemarrerUnSejourCommand.label;
    }

    public String getReservationId() {
        return reservationId;
    }

    public LocalDateTime getDateDeDebut() {
        return dateDeDebut;
    }

    public boolean equals(Object o) {
        if (this==o) return true;
        if (o==null || getClass()!=o.getClass()) return false;
        DemarrerUnSejourCommand that = (DemarrerUnSejourCommand) o;
        return Objects.equals(reservationId, that.reservationId) && Objects.equals(dateDeDebut, that.dateDeDebut);
    }
}
