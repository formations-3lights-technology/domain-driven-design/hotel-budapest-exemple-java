package com.threelightstechnology.hotelbudapest.deroulement_sejour.use_cases.finir_sejour.commad_handler;

import com.threelightstechnology.hotelbudapest.shared.command_disptacher.Command;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Objects;
import java.util.Optional;

public class FinirUnSejourCommand extends Command {
    public static final String label = "FinishTripCommand";

    private final String sejourId;
    private final LocalDateTime dateDeFin;
    private final BigDecimal montant;
    private final Optional<String> mailDeFacturation;

    public FinirUnSejourCommand(
            String sejourId,
            LocalDateTime dateDeFin,
            BigDecimal montant,
            Optional<String> mailDeFacturation
    ) {
        super();
        this.sejourId = sejourId;
        this.dateDeFin = dateDeFin;
        this.montant = montant;
        this.mailDeFacturation = mailDeFacturation;
    }

    public String label() {
        return FinirUnSejourCommand.label;
    }

    public String getSejourId() {
        return sejourId;
    }

    public LocalDateTime getDateDeFin() {
        return dateDeFin;
    }

    public BigDecimal getMontant() {
        return montant;
    }

    public Optional<String> getMailDeFacturation() {
        return mailDeFacturation;
    }

    public boolean equals(Object o) {
        if (this==o) return true;
        if (o==null || getClass()!=o.getClass()) return false;
        FinirUnSejourCommand that = (FinirUnSejourCommand) o;
        return Objects.equals(sejourId, that.sejourId) && Objects.equals(dateDeFin, that.dateDeFin) && Objects.equals(montant, that.montant) && Objects.equals(mailDeFacturation, that.mailDeFacturation);
    }
}
