package com.threelightstechnology.hotelbudapest.deroulement_sejour.use_cases;

import com.threelightstechnology.hotelbudapest.deroulement_sejour.domain.sejour.SejourRepository;
import com.threelightstechnology.hotelbudapest.deroulement_sejour.domain.sejour.SejourSnapshot;
import org.springframework.stereotype.Service;

@Service
public class GetSejourParId {
    private final SejourRepository sejourRepository;

    public GetSejourParId(SejourRepository sejourRepository) {
        this.sejourRepository = sejourRepository;
    }

    public SejourSnapshot handle(String id) {
        return sejourRepository.getParIdOuErreur(id);
    }
}
