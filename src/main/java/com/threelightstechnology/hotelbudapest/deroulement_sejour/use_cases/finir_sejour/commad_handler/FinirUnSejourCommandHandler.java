package com.threelightstechnology.hotelbudapest.deroulement_sejour.use_cases.finir_sejour.commad_handler;

import com.threelightstechnology.hotelbudapest.deroulement_sejour.domain.sejour.Sejour;
import com.threelightstechnology.hotelbudapest.deroulement_sejour.domain.sejour.SejourRepository;
import com.threelightstechnology.hotelbudapest.deroulement_sejour.domain.sejour.SejourSnapshot;
import com.threelightstechnology.hotelbudapest.shared.command_disptacher.CommandHandler;
import com.threelightstechnology.hotelbudapest.shared.event_publisher.EventPublisher;

public class FinirUnSejourCommandHandler implements CommandHandler<FinirUnSejourCommand> {
    private final SejourRepository sejourRepository;
    private final EventPublisher eventPublisher;

    public FinirUnSejourCommandHandler(
            SejourRepository sejourRepository,
            EventPublisher eventPublisher
    ) {
        this.sejourRepository = sejourRepository;
        this.eventPublisher = eventPublisher;
    }

    public void handle(FinirUnSejourCommand command) {
        SejourSnapshot snapshot = sejourRepository.getParIdOuErreur(command.getSejourId());
        Sejour sejour = Sejour.restore(snapshot);

        sejour.terminer(command.getDateDeFin(), command.getMailDeFacturation().orElse(null), command.getMontant());

        sejourRepository.save(sejour.getSnapshot());
        eventPublisher.publish(sejour.getRaisedEvents());
    }
}
