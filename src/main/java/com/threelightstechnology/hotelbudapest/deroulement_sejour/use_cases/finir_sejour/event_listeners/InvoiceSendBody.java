package com.threelightstechnology.hotelbudapest.deroulement_sejour.use_cases.finir_sejour.event_listeners;

import java.math.BigDecimal;

public record InvoiceSendBody(String invoiceEmail, BigDecimal amount) {
    public String toString() {
        return "{" +
                "invoiceEmail='" + invoiceEmail + '\'' +
                ", amount=" + amount +
                '}';
    }
}
