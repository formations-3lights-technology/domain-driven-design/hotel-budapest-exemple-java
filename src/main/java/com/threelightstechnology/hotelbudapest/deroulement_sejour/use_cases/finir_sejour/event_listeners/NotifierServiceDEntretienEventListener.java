package com.threelightstechnology.hotelbudapest.deroulement_sejour.use_cases.finir_sejour.event_listeners;

import com.threelightstechnology.hotelbudapest.deroulement_sejour.domain.sejour.events.SejourTermineEvent;
import com.threelightstechnology.hotelbudapest.shared.event_publisher.EventListener;

public class NotifierServiceDEntretienEventListener implements EventListener<SejourTermineEvent> {
    public void listen(SejourTermineEvent event) {
        System.out.println("Service d'entretien notifié");
    }
}
