package com.threelightstechnology.hotelbudapest.deroulement_sejour.use_cases.demarrer_sejour;

import com.threelightstechnology.hotelbudapest.deroulement_sejour.domain.reservation.Reservation;
import com.threelightstechnology.hotelbudapest.deroulement_sejour.domain.reservation.ReservationLoader;
import com.threelightstechnology.hotelbudapest.deroulement_sejour.domain.sejour.Sejour;
import com.threelightstechnology.hotelbudapest.deroulement_sejour.domain.sejour.SejourRepository;
import com.threelightstechnology.hotelbudapest.shared.command_disptacher.CommandHandler;
import com.threelightstechnology.hotelbudapest.shared.event_publisher.EventPublisher;

public class DemarrerUnSejourCommandHandler implements CommandHandler<DemarrerUnSejourCommand> {
    private final SejourRepository sejourRepository;
    private final ReservationLoader reservationLoader;
    private final EventPublisher eventPublisher;

    public DemarrerUnSejourCommandHandler(
            SejourRepository sejourRepository,
            ReservationLoader reservationLoader,
            EventPublisher eventPublisher
    ) {
        this.sejourRepository = sejourRepository;
        this.reservationLoader = reservationLoader;
        this.eventPublisher = eventPublisher;
    }

    public void handle(DemarrerUnSejourCommand command) {
        Reservation reservation = reservationLoader.getParIdOuErreur(command.getReservationId());

        Sejour sejour = Sejour.enregistrement(
                sejourRepository.nextId(),
                reservation.id(),
                reservation.prixParNuit(),
                command.getDateDeDebut(),
                reservation.dateDeDebut(),
                reservation.dateDeFin()
        );

        sejourRepository.save(sejour.getSnapshot());
        eventPublisher.publish(sejour.getRaisedEvents());
    }
}
