package com.threelightstechnology.hotelbudapest.deroulement_sejour.use_cases.envoyer_questionnaire_de_satisfaction;

import com.threelightstechnology.hotelbudapest.shared.command_disptacher.Command;

import java.time.LocalDateTime;

public class EnvoyerQuestionnaireDeSatisfactionCommand extends Command {
    public static final String label = "SendSatisfactionSurveyCommand";
    private final LocalDateTime date;

    public EnvoyerQuestionnaireDeSatisfactionCommand(LocalDateTime date) {
        super();
        this.date = date;
    }

    public String label() {
        return EnvoyerQuestionnaireDeSatisfactionCommand.label;
    }

    public LocalDateTime getDate() {
        return date;
    }
}
