package com.threelightstechnology.hotelbudapest.deroulement_sejour.use_cases.envoyer_questionnaire_de_satisfaction;

import com.threelightstechnology.hotelbudapest.deroulement_sejour.domain.reservation.Reservation;
import com.threelightstechnology.hotelbudapest.deroulement_sejour.domain.reservation.ReservationLoader;
import com.threelightstechnology.hotelbudapest.deroulement_sejour.domain.sejour.SejourRepository;
import com.threelightstechnology.hotelbudapest.deroulement_sejour.domain.sejour.SejourSnapshot;
import com.threelightstechnology.hotelbudapest.facturation.domain.Mail;
import com.threelightstechnology.hotelbudapest.facturation.domain.MailSender;
import com.threelightstechnology.hotelbudapest.shared.command_disptacher.CommandHandler;

import java.util.List;

public class EnvoyerQuestionnaireDeSatisfactionCommandHandler implements CommandHandler<EnvoyerQuestionnaireDeSatisfactionCommand> {
    private final SejourRepository sejourRepository;
    private final ReservationLoader reservationLoader;
    private final MailSender mailSender;

    public EnvoyerQuestionnaireDeSatisfactionCommandHandler(
            SejourRepository sejourRepository,
            ReservationLoader reservationLoader,
            MailSender mailSender
    ) {
        this.sejourRepository = sejourRepository;
        this.reservationLoader = reservationLoader;
        this.mailSender = mailSender;
    }

    public void handle(EnvoyerQuestionnaireDeSatisfactionCommand command) {
        List<SejourSnapshot> sejours = sejourRepository.getParDateDeFin(command.getDate().toLocalDate());

        for (SejourSnapshot sejour : sejours) {
            Reservation reservation = reservationLoader.getParIdOuErreur(sejour.reservationId());
            mailSender.send(
                    new Mail(
                            "relation-client@email.com",
                            reservation.mailDeReservation(),
                            "Questionnaire de satisfaction"
                    )
            );
        }
    }
}
