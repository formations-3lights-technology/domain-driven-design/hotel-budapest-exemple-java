package com.threelightstechnology.hotelbudapest.deroulement_sejour.use_cases.finir_sejour.event_listeners;

import com.threelightstechnology.hotelbudapest.deroulement_sejour.domain.reservation.ReservationLoader;
import com.threelightstechnology.hotelbudapest.deroulement_sejour.domain.sejour.events.EnvoiFactureDemandeEvent;
import com.threelightstechnology.hotelbudapest.shared.domain.http_client.HttpClient;
import com.threelightstechnology.hotelbudapest.shared.event_publisher.EventListener;

import java.util.Optional;

public class EnvoyerUneFactureEventListener implements EventListener<EnvoiFactureDemandeEvent> {
    private final ReservationLoader reservationLoader;
    private final HttpClient httpClient;

    public EnvoyerUneFactureEventListener(
            ReservationLoader reservationLoader,
            HttpClient httpClient
    ) {
        this.reservationLoader = reservationLoader;
        this.httpClient = httpClient;
    }

    public void listen(EnvoiFactureDemandeEvent event) {
        String emailDeFacturation = event.getMailDeFacturation()
                .orElseGet(() -> reservationLoader.getParIdOuErreur(event.getReservationId()).mailDeReservation());

        httpClient.post(
                "/invoices/send",
                Optional.of(new InvoiceSendBody(emailDeFacturation, event.getAmount()).toString())
        );
    }
}
