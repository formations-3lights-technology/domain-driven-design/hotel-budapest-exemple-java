package com.threelightstechnology.hotelbudapest.shared.domain.erreurs;

public class NonAutorisation extends RuntimeException {
    public NonAutorisation(String message) {
        super(message);
    }
}
