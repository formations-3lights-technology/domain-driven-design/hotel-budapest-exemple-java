package com.threelightstechnology.hotelbudapest.shared.domain.erreurs;

public class ElementNonTrouve extends RuntimeException {
    public ElementNonTrouve(String message) {
        super(message);
    }
}
