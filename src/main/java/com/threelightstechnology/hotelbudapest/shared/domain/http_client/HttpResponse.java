package com.threelightstechnology.hotelbudapest.shared.domain.http_client;

public record HttpResponse<R>(R body) {
}
