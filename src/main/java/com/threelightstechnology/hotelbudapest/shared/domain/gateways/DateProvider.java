package com.threelightstechnology.hotelbudapest.shared.domain.gateways;

import java.time.LocalDateTime;

public interface DateProvider {
    LocalDateTime now();
}
