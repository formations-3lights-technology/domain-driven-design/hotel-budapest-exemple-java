package com.threelightstechnology.hotelbudapest.shared.domain.http_client;


import java.util.Optional;

public interface HttpClient {
    <R> HttpResponse<R> get(String url);

    void post(String url, Optional<String> body);
}
