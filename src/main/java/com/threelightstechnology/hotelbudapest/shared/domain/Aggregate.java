package com.threelightstechnology.hotelbudapest.shared.domain;

import com.threelightstechnology.hotelbudapest.shared.event_publisher.DomainEvent;

import java.util.ArrayList;
import java.util.List;

public class Aggregate {
    protected List<DomainEvent> events = new ArrayList<>();

    public List<DomainEvent> getRaisedEvents() {
        return this.events;
    }

    protected void apply(DomainEvent event) {
        this.events.add(event);
    }
}
