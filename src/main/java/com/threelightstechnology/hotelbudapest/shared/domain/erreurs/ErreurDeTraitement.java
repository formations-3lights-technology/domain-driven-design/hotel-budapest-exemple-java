package com.threelightstechnology.hotelbudapest.shared.domain.erreurs;

public class ErreurDeTraitement extends RuntimeException {
    public ErreurDeTraitement(String message) {
        super(message);
    }
}
