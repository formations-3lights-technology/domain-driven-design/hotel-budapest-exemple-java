package com.threelightstechnology.hotelbudapest.shared.event_publisher;

public abstract class DomainEvent {
    public abstract String label();
}
