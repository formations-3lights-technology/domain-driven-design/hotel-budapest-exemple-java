package com.threelightstechnology.hotelbudapest.shared.event_publisher;

import java.util.List;
import java.util.Map;
import java.util.Set;

public interface EventPublisher {
    EventPublisher registerListeners(Map<String, Set<EventListener<?>>> listeners);

    void publish(List<DomainEvent> events);
}
