package com.threelightstechnology.hotelbudapest.shared.event_publisher;

public interface EventListener<E extends DomainEvent> {
    void listen(E event);
}
