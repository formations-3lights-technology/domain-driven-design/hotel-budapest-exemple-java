package com.threelightstechnology.hotelbudapest.shared.event_publisher;

import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

public class InternalEventPublisher implements EventPublisher {
    private Map<String, Set<EventListener<? extends DomainEvent>>> listeners = Map.of();

    public EventPublisher registerListeners(Map<String, Set<EventListener<?>>> listeners) {
        this.listeners = listeners;
        return this;
    }

    @SuppressWarnings("unchecked")
    public void publish(List<DomainEvent> events) {
        for (DomainEvent event : events) {
            Set<EventListener<? extends DomainEvent>> eventListeners1 = listeners.get(event.label());

            if (eventListeners1 == null) {
                continue;
            }

            Set<EventListener<DomainEvent>> eventListeners = eventListeners1.stream()
                    .map(l -> (EventListener<DomainEvent>) l)
                    .collect(Collectors.toSet());

            for (EventListener<DomainEvent> eventListener : eventListeners) {
                try {
                    eventListener.listen(event);
                } catch (Exception e) {
                    System.out.println(e.getMessage());
                }
            }
        }
    }
}
