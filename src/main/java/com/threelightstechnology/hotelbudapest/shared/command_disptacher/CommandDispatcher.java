package com.threelightstechnology.hotelbudapest.shared.command_disptacher;

import java.util.Map;

public interface CommandDispatcher {
    CommandDispatcher registerHandlers(Map<String, CommandHandler<?>> handlers);

    void dispatch(Command command);
}
