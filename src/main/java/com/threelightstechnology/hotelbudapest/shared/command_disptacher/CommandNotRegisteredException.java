package com.threelightstechnology.hotelbudapest.shared.command_disptacher;

public class CommandNotRegisteredException extends RuntimeException {
    public CommandNotRegisteredException() {
        super("The dispatched command is not registered");
    }
}
