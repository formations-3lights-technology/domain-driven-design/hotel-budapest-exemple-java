package com.threelightstechnology.hotelbudapest.shared.command_disptacher;

public abstract class Command {
    public abstract String label();
}
