package com.threelightstechnology.hotelbudapest.shared.command_disptacher;

public interface CommandHandler<C extends Command> {
    void handle(C command);
}
