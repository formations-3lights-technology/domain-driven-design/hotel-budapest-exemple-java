package com.threelightstechnology.hotelbudapest.shared.command_disptacher;

import java.util.Map;

public class InternalCommandDispatcher implements CommandDispatcher {
    private Map<String, CommandHandler<? extends Command>> handlers = Map.of();

    public CommandDispatcher registerHandlers(Map<String, CommandHandler<?>> handlers) {
        this.handlers = handlers;
        return this;
    }

    @SuppressWarnings("unchecked")
    public void dispatch(Command command) {
        CommandHandler<Command> handler = (CommandHandler<Command>) handlers.get(command.label());

        if (handler==null) {
            throw new CommandNotRegisteredException();
        }

        handler.handle(command);
    }
}
