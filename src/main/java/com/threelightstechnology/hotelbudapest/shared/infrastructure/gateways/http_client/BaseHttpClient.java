package com.threelightstechnology.hotelbudapest.shared.infrastructure.gateways.http_client;

import com.threelightstechnology.hotelbudapest.shared.domain.http_client.HttpClient;
import com.threelightstechnology.hotelbudapest.shared.domain.http_client.HttpResponse;
import org.springframework.web.reactive.function.client.WebClient;

import java.util.Optional;

public class BaseHttpClient implements HttpClient {
    protected final WebClient webHttpClient;

    public BaseHttpClient(String baseUrl) {
        webHttpClient = WebClient
                .builder()
                .baseUrl(baseUrl)
                .build();
    }

    public <R> HttpResponse<R> get(String url) {
        return new HttpResponse<>(null);
    }

    public void post(String url, Optional<String> body) {
    }
}
