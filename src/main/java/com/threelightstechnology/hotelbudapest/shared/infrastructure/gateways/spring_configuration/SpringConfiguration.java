package com.threelightstechnology.hotelbudapest.shared.infrastructure.gateways.spring_configuration;

import org.springframework.context.annotation.Configuration;

@Configuration
public class SpringConfiguration {
    private final ConfigurationRoomBookingService configurationRoomBookingService;
    private final ConfigurationInvoicingService configurationInvoicingService;

    public SpringConfiguration(
            ConfigurationRoomBookingService configurationRoomBookingService,
            ConfigurationInvoicingService configurationInvoicingService
    ) {
        this.configurationRoomBookingService = configurationRoomBookingService;
        this.configurationInvoicingService = configurationInvoicingService;
    }

    public ConfigurationRoomBookingService getConfigurationRoomBookingService() {
        return configurationRoomBookingService;
    }

    public ConfigurationInvoicingService getConfigurationInvoicingService() {
        return configurationInvoicingService;
    }
}
