package com.threelightstechnology.hotelbudapest.shared.infrastructure.gateways.http_client;

import com.threelightstechnology.hotelbudapest.shared.infrastructure.gateways.spring_configuration.SpringConfiguration;
import org.springframework.stereotype.Component;

@Component
public class InvoicingServiceHttpClient extends BaseHttpClient {
    public InvoicingServiceHttpClient(SpringConfiguration configuration) {
        super(configuration.getConfigurationInvoicingService().getBaseUrl());
    }
}
