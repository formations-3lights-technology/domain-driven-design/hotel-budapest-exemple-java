package com.threelightstechnology.hotelbudapest.shared.infrastructure.gateways.http_client;

import com.threelightstechnology.hotelbudapest.shared.infrastructure.gateways.spring_configuration.SpringConfiguration;
import org.springframework.stereotype.Component;

@Component
public class RoomBookingServiceHttpClient extends BaseHttpClient {
    public RoomBookingServiceHttpClient(SpringConfiguration configuration) {
        super(configuration.getConfigurationRoomBookingService().getBaseUrl());
    }
}
