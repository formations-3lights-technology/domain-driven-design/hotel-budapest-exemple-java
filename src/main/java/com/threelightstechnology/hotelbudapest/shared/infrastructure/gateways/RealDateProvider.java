package com.threelightstechnology.hotelbudapest.shared.infrastructure.gateways;

import com.threelightstechnology.hotelbudapest.shared.domain.gateways.DateProvider;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;

@Component
public class RealDateProvider implements DateProvider {
    public LocalDateTime now() {
        return LocalDateTime.now();
    }
}
