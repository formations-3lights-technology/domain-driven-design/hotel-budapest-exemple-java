package com.threelightstechnology.hotelbudapest.shared.infrastructure.entrypoints.endpoints;

import com.threelightstechnology.hotelbudapest.shared.domain.erreurs.ElementNonTrouve;
import com.threelightstechnology.hotelbudapest.shared.domain.erreurs.ErreurDeValidation;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import java.time.ZonedDateTime;
import java.util.Arrays;

@ControllerAdvice
public class ControllerErrorHandler {
    @ExceptionHandler({Exception.class, Throwable.class})
    ResponseEntity<RestError> handle(Exception exception) {
        return createResponse(exception, HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @ExceptionHandler(ErreurDeValidation.class)
    ResponseEntity<RestError> handle(ErreurDeValidation exception) {
        return createResponse(exception, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(ElementNonTrouve.class)
    ResponseEntity<RestError> handle(ElementNonTrouve exception) {
        return createResponse(exception, HttpStatus.NOT_FOUND);
    }

    private ResponseEntity<RestError> createResponse(Exception exception, HttpStatus statusCode) {
        RestError error = new RestError(
                statusCode.name(),
                exception.getMessage(),
                ZonedDateTime.now()
        );
        return ResponseEntity.status(statusCode).body(error);
    }

    record RestError(String statusCode, String message, ZonedDateTime timeStamp) {
    }
}
