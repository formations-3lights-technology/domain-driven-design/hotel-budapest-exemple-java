set
search_path to deroulement_sejour;

create table sejour
(
    id                    varchar                  not null
        constraint sejour_pk
            primary key,
    reservation_id        varchar                  not null,
    prix_par_nuit         integer                  not null,
    date_d_enregistrement timestamp with time zone not null,
    date_de_fin_prevue    timestamp with time zone not null,
    date_de_fin           timestamp with time zone,
    montant_paye          integer
);
