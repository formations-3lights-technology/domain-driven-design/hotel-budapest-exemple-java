set search_path to chambre_reservation;

create table reservation
(
    id                   varchar                  not null
        constraint reservation_pk
            primary key,
    numero_de_la_chambre varchar                  not null,
    mail_de_reservation  varchar                  not null,
    nombre_de_personnes  integer                  not null,
    prix_par_nuit        integer                  not null,
    date_de_debut        timestamp with time zone not null,
    date_de_fin          timestamp with time zone not null
);
