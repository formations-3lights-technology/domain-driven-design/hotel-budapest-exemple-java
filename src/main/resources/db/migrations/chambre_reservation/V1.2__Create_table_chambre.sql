set search_path to chambre_reservation;

create table chambre
(
    numero        varchar not null
        constraint chambre_pk
            primary key,
    nom           varchar not null,
    capacite      integer not null,
    prix_par_nuit integer not null
);

insert into chambre (numero, nom, capacite, prix_par_nuit)
values ('11', 'Chambre Standard', 2, 80);
insert into chambre (numero, nom, capacite, prix_par_nuit)
values ('12', 'Chambre Familiale', 4, 150);
insert into chambre (numero, nom, capacite, prix_par_nuit)
values ('14', 'Chambre vue Jardin', 3, 170);
insert into chambre (numero, nom, capacite, prix_par_nuit)
values ('21', 'Chambre Standard', 2, 80);
insert into chambre (numero, nom, capacite, prix_par_nuit)
values ('22', 'Suite Familiale', 5, 285);
insert into chambre (numero, nom, capacite, prix_par_nuit)
values ('23', 'Petite Chambre', 1, 70);
insert into chambre (numero, nom, capacite, prix_par_nuit)
values ('31', 'Chambre Standard', 2, 80);
insert into chambre (numero, nom, capacite, prix_par_nuit)
values ('32', 'Chambre Deluxe vue Jardin', 3, 235);
